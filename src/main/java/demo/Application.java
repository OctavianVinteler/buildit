package demo;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.web.WebMvcProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import demo.integration.RequestGateway;
import demo.services.rentit.CustomResponseErrorHandler;

@Configuration
@ComponentScan
@EnableAutoConfiguration
@ImportResource({"classpath:META-INF/integration/integration.xml", "classpath:META-INF/integration/integration-mail.xml"})
public class Application {
	@Autowired
	private WebMvcProperties mvcProperties = new WebMvcProperties();
	@Autowired
	ClientHttpRequestFactory factory;
	//////////////REST Beans///////////////////////////////////////////////////
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate _restTemplate = new RestTemplate();
		List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
		messageConverters.add(new MappingJackson2HttpMessageConverter());
		_restTemplate.setMessageConverters(messageConverters);
		_restTemplate.setErrorHandler(new CustomResponseErrorHandler());
		_restTemplate.setRequestFactory(factory);
		return _restTemplate;
	}
	
	public class BasicSecureSimpleClientHttpRequestFactory extends SimpleClientHttpRequestFactory {
	    @Autowired
	    Credentials credentials;

	    public BasicSecureSimpleClientHttpRequestFactory() {
	    }

	    @Override
	    public ClientHttpRequest createRequest(URI uri, HttpMethod httpMethod) throws IOException {
	        ClientHttpRequest result = super.createRequest(uri, httpMethod);
	        System.out.println(uri);
	        System.out.println(uri.getAuthority());
	        System.out.println(credentials.getCredentials());

	        for (Map<String, String> map: credentials.getCredentials().values()) {
	            String authority = map.get("authority");
	            if (authority != null && authority.equals(uri.getAuthority())) {
	                result.getHeaders().add("Authorization", map.get("authorization"));
	                break;
	            }
	        }

	        if (credentials.getCredentials().containsKey(uri.getAuthority())) {
	        }
	        return result;
	    }
	}

	@Bean
	public ClientHttpRequestFactory requestFactory() {
	    return new BasicSecureSimpleClientHttpRequestFactory();
	}

	@Bean
	@ConfigurationProperties(locations="classpath:META-INF/integration/credentials.yml")
	public Credentials getCredentials() {
	    return new Credentials();
	}

	public static class Credentials {
	    private Map<String, Map<String, String>> credentials = new HashMap<>();
	    public Map<String, Map<String, String>> getCredentials() {
	        return this.credentials;
	    }
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
		//ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
		
		//Credentials creds = ctx.getBean(Credentials.class);
		
		//RequestGateway proxy = ctx.getBean(RequestGateway.class);
        
        //System.out.println(proxy.getAllPlants());*/
	}
}
