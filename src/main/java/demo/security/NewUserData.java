package demo.security;

import lombok.Data;

@Data
public class NewUserData {
	private String username;
	private String password;
	private String secondPassword;
	private String role;
}
