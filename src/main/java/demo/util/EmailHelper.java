package demo.util;

import java.io.File;
import java.io.InputStream;

import javax.mail.internet.MimeMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import demo.integration.dto.RemittanceAdviceResource;
import demo.models.RemittanceAdvice;
import demo.services.rentit2.RemittanceAdviceRepresentation;
import demo.services.rentit3.RemittanceAdviceRepresentation2;

public class EmailHelper {
	
	public void sendEmail(RemittanceAdvice ra, JavaMailSenderImpl sender, String address, String fileName) throws Exception
	{
		MimeMessage message = sender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(address);

		helper.setText("Remittance Advice");
		
		JAXBContext context = JAXBContext.newInstance(RemittanceAdvice.class);
        Marshaller m = context.createMarshaller();
        //for pretty-print XML in JAXB
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to File
        File file = new File("remittanceAdvice.xml");
        m.marshal(ra, file);
		
		helper.addAttachment(fileName, file);

		sender.send(message);
	}
	
	public void sendEmail(RemittanceAdviceRepresentation ra, JavaMailSenderImpl sender, String address, String fileName) throws Exception
	{
		MimeMessage message = sender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(address);

		helper.setText("Remittance Advice");
		
		JAXBContext context = JAXBContext.newInstance(RemittanceAdviceRepresentation.class);
        Marshaller m = context.createMarshaller();
        //for pretty-print XML in JAXB
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to File
        File file = new File("remittanceAdvice.xml");
        m.marshal(ra, file);
        
        InputStream str = FileUtils.openInputStream(file);
		
		helper.addAttachment(fileName, new ByteArrayResource(IOUtils.toByteArray(str)), "application/xml");

		sender.send(message);
	}
	
	public void sendEmail(RemittanceAdviceRepresentation2 ra, JavaMailSenderImpl sender, String address, String fileName) throws Exception
	{
		MimeMessage message = sender.createMimeMessage();
		
		MimeMessageHelper helper = new MimeMessageHelper(message, true);
		helper.setTo(address);

		helper.setText("Remittance Advice");
		
		JAXBContext context = JAXBContext.newInstance(RemittanceAdviceRepresentation2.class);
        Marshaller m = context.createMarshaller();
        //for pretty-print XML in JAXB
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        // Write to File
        File file = new File("remittanceAdvice.xml");
        m.marshal(ra, file);
        
        InputStream str = FileUtils.openInputStream(file);
		
		helper.addAttachment(fileName, new ByteArrayResource(IOUtils.toByteArray(str)), "application/xml");

		sender.send(message);
	}
}
