package demo.models;

public enum PHRStatus {
	PENDING, 
	APPROVED, 
	REJECTED,
	ENDED,
	EXTENSION_PENDING,
	CANCELLED
}
