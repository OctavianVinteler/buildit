package demo.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;

@Entity
@Data
public class PlantHireRequest {

	@Id
	@GeneratedValue
	Long id;
	
	@OneToOne
	SiteEngineer siteEngineer;
	@OneToOne
	WorkEngineer workEngineer;
	
	@OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="phr_id")
	List<PlantHireRequestLine> lines;
	
	Float totalPrice;
	
	int linesNumber;
}
