package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import demo.models.PlantHireRequestExtension;

@Repository
public interface PHRExtensionRepository extends JpaRepository<PlantHireRequestExtension, Long>  {

}
