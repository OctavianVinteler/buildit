package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import demo.models.Invoice;

public interface InvoiceRepository extends JpaRepository<Invoice, Long> {

	@Query("select i from Invoice i " +
	           "where LOWER(i.purchaseOrderRef) like LOWER(:poRef)")
	    Invoice finderMethod(@Param("poRef") String poRef);
}
