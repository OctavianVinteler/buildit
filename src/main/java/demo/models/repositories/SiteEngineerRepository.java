package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.models.SiteEngineer;

public interface SiteEngineerRepository extends JpaRepository<SiteEngineer, Long> {

}
