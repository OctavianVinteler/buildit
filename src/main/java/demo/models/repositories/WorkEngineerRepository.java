package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.models.WorkEngineer;

public interface WorkEngineerRepository extends JpaRepository<WorkEngineer, Long> {

}
