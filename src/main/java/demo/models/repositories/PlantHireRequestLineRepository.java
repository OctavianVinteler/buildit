package demo.models.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import demo.models.PlantHireRequestLine;

@Repository
public interface PlantHireRequestLineRepository extends JpaRepository<PlantHireRequestLine, Long> {

	@Query("select p from PlantHireRequestLine p " +
	           "where LOWER(p.purchaseOrderRef) like LOWER(:poRef)")
	    List<PlantHireRequestLine> finderMethod(@Param("poRef") String poRef);
}
