package demo.models.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import demo.models.PlantHireRequest;

public interface PlantHireRequestRepository extends JpaRepository<PlantHireRequest, Long>{

}
