package demo.models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class PlantHireRequestExtension {

	@Id
    @GeneratedValue
    Long id;
	
	@Temporal(TemporalType.DATE)
    Date endDate;
	
	@Enumerated(EnumType.STRING)
	ExtensionStatus status;
}
