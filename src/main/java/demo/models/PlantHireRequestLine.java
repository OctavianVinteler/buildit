package demo.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Data;

@Entity
@Data
public class PlantHireRequestLine {	
	@Id
	@GeneratedValue
	Long id;
	
	String plantRef;
	String purchaseOrderRef;
	
	@Temporal(TemporalType.DATE)
	Date startDate;
	
	@Temporal(TemporalType.DATE)
	Date endDate;
	
	Float price;
	
	@Enumerated(EnumType.STRING)
	PHRStatus status;
	
	@OneToMany(cascade=CascadeType.ALL)
    @JoinColumn(name="phrl_id")
	List<PlantHireRequestExtension> extension;
}
