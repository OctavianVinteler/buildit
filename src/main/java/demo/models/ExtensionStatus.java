package demo.models;

public enum ExtensionStatus {
	PENDING,
	APPROVED,
	REJECTED
}
