package demo.services.rentit3;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.services.rentit2.PurchaseOrderRepresentation;
import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "invoiceResource")
public class InvoiceRepresentation2 extends ResourceSupport {

	PurchaseOrderRepresentation purchaseOrder;
	Float total;
	//@DateTimeFormat(iso = ISO.DATE)
	Date dueDate;
}
