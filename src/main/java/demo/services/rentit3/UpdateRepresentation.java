package demo.services.rentit3;

import java.util.Date;

import lombok.Data;
import demo.services.rentit2.CustomerRepresentation;
import demo.services.rentit2.PlantRepresentation;

@Data
public class UpdateRepresentation {
	Long idRes;
	PlantRepresentation plant;
	//CustomerRepresentation customer;
	Date startDate;
	Date endDate;
	//Float cost;
}
