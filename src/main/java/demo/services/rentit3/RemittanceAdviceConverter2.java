package demo.services.rentit3;

import demo.models.RemittanceAdvice;

public class RemittanceAdviceConverter2 {

	public RemittanceAdviceRepresentation2 reverseTransform(RemittanceAdvice ra, Long purchaseOrderId)
	{
		RemittanceAdviceRepresentation2 rep = new RemittanceAdviceRepresentation2();
		rep.setPurchaseOrderId(purchaseOrderId);
		rep.setTotal(ra.getAmountPayed());
		
		return rep;
	}
}
