package demo.services.rentit3;

import java.util.Date;

import demo.integration.dto.PurchaseOrderResource;
import demo.services.rentit2.CustomerRepresentation;
import demo.services.rentit2.PlantConverter;
import demo.util.DateHelper;

public class UpdateConverter {
	
	PlantConverter plantConverter = new PlantConverter();

	public UpdateRepresentation reverseTransform(PurchaseOrderResource res, Date newEndDate)
	{
		UpdateRepresentation update = new UpdateRepresentation();
		update.setIdRes(res.getPoId());
		update.setPlant(plantConverter.reverseTransform(res.getPlant()));
		//update.setCost(res.getCost());
		update.setStartDate(DateHelper.addDays(res.getStartDate(), 1));
		update.setEndDate(newEndDate);
		//update.setCustomer(new CustomerRepresentation());
		return update;
	}
}
