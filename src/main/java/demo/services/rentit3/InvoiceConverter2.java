package demo.services.rentit3;

import demo.models.Invoice;

public class InvoiceConverter2 {
	public Invoice transform(InvoiceRepresentation2 invoice)
	{
		Invoice inv = new Invoice();
		//inv.setId(invoice.getIdRes());
		inv.setDueDate(invoice.getDueDate());
		inv.setEndDate(invoice.getPurchaseOrder().getEndDate());
		inv.setStartDate(invoice.getPurchaseOrder().getStartDate());
		inv.setPurchaseOrderRef(invoice.getPurchaseOrder().getLink("self").getHref());
		inv.setTotal(invoice.getTotal());
		inv.setLatePayment(false);
		inv.setPayed(false);
		
		return inv;
	}
}
