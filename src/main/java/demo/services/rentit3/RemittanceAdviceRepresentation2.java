package demo.services.rentit3;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "remittanceAdviceResource")
public class RemittanceAdviceRepresentation2 {
	Long purchaseOrderId;
	Float total;
}
