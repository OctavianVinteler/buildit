package demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.PHRStatus;
import demo.models.PlantHireRequestLine;
import demo.models.repositories.PlantHireRequestLineRepository;
import demo.services.rentit.RentalService;

@Service
public class PlantHireRequestManager {
    @Autowired
    PlantHireRequestLineRepository phrRepository;
    @Autowired
    RentalService rentitProxy;

	public PlantHireRequestLine createPurchaseOrder(Long id) throws PlantNotAvailableException, RestClientException {
		PlantHireRequestLine phr = phrRepository.findOne(id);

		PlantResource plantRS = new PlantResource();
		plantRS.add(new Link(phr.getPlantRef()));
		
		PurchaseOrderResource po = rentitProxy.createPurchaseOrder(plantRS, phr.getStartDate(), phr.getEndDate());
		
		phr.setPurchaseOrderRef(po.getId().getHref());
		phr.setPrice(po.getCost());
		phrRepository.save(phr);
		return phr;
	}

	public PlantHireRequestLine createPlantHireRequest(PlantHireRequestLine phr) {
		phr.setId(null);
		phr.setStatus(PHRStatus.PENDING);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine updatePlantHireRequest(PlantHireRequestLine phr){
		phr.setStatus(PHRStatus.PENDING);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine approvePlantHireRequest(Long id, String poRef) {
		PlantHireRequestLine phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.APPROVED);
		phr.setPurchaseOrderRef(poRef);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine rejectPlantHireRequest(Long id) {
		PlantHireRequestLine phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.REJECTED);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine endPlantHireRequest(Long id){
		PlantHireRequestLine phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.ENDED);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine requestExtensionPlantHireRequest(Long id){
		PlantHireRequestLine phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.EXTENSION_PENDING);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine rejectExtensionPlantHireRequest(Long phrid, Long exid){
		PlantHireRequestLine phr = phrRepository.findOne(phrid);
		phr.setStatus(PHRStatus.APPROVED);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine approveExtensionPlantHireRequest(Long phrid, Long exid){
		PlantHireRequestLine phr = phrRepository.findOne(phrid);
		phr.setStatus(PHRStatus.APPROVED);
		
		phrRepository.save(phr);
		return phr;
	}
	
	public PlantHireRequestLine cancelPlantHireRequest(Long id){
		PlantHireRequestLine phr = phrRepository.findOne(id);
		phr.setStatus(PHRStatus.CANCELLED);
		
		phrRepository.save(phr);
		return phr;
	}

	public PlantHireRequestLine getPlantHireRequest(Long id) {
		return phrRepository.findOne(id);
	}
	
	public List<PlantHireRequestLine> getAllPlantHireRequests(){
		return phrRepository.findAll();
	}
}
