package demo.services.rentit2;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Data;

@Data
@XmlRootElement(name = "rm")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "idRes", "invoiceId", "paymentDate" })
public class RemittanceAdviceRepresentation {
	Long idRes;
	Long invoiceId;
	Date paymentDate;
}
