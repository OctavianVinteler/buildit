package demo.services.rentit2;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.util.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@XmlRootElement(name="purchaseOrder")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseOrderRepresentation extends ResourceSupport {
	Long idRes;
    PlantRepresentation plant;
    Date startDate;
    Date endDate;
    Float cost;
    CustomerRepresentation customer;
}
