package demo.services.rentit2;

import java.util.Date;

import demo.models.Invoice;

public class InvoiceConverter {
	public Invoice transform(InvoiceRepresentation invoice)
	{
		Invoice inv = new Invoice();
		inv.setId(invoice.getIdRes());
		inv.setDueDate(invoice.getDate());
		inv.setEndDate(new Date());
		inv.setStartDate(new Date());
		inv.setPurchaseOrderRef(invoice.getPurchaseOrderRef());
		inv.setTotal(invoice.getTotal());
		inv.setLatePayment(false);
		inv.setPayed(false);
		inv.setOriginalId(invoice.getIdRes());
		/*if(invoice.getInvoiceStatus() == InvoiceStatus.NOT_PAID)
		{
			inv.setPayed(false);
		}
		else
		{
			inv.setPayed(true);
		}*/
		
		return inv;
	}
}
