package demo.services.rentit2;

import lombok.Getter;
import lombok.Setter;
import demo.util.ResourceSupport;

@Setter
@Getter
public class CustomerRepresentation extends ResourceSupport {
	Long idRes;
	String name;
    String vatNumber;
    
    public CustomerRepresentation()
    {
    	idRes = 111L;
    	name = "rentit-team4";
    	vatNumber = "loremIpsum";
    }
}

