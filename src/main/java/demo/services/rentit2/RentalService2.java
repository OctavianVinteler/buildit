package demo.services.rentit2;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.services.PlantNotAvailableException;

@Service
public class RentalService2 {

	@Autowired
	RestTemplate restTemplate;
	
	PlantConverter plantConverter = new PlantConverter();
	PurchaseOrderConverter poConverter = new PurchaseOrderConverter();
	
	String host = "rentit-team3.herokuapp.com";
	
	String port = "80";
	
	public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		PlantRepresentation[] plants = restTemplate.getForObject(
				"http://" + host + ":" + port + "/rest/plants?name={name}&startDate={start}&endDate={end}",
				PlantRepresentation[].class, 
				plantName,
				formatter.format(startDate),
				formatter.format(endDate));
		return plantConverter.transform(Arrays.asList(plants));
	}
	
	public PlantResource getPlant(String link) {
		ResponseEntity<PlantRepresentation> result = restTemplate.getForEntity(link, PlantRepresentation.class);
		
		return plantConverter.transform(result.getBody());
	}
	
	public PurchaseOrderResource createPurchaseOrder(PlantResource plant, Date startDate, Date endDate) throws RestClientException, PlantNotAvailableException {
		PurchaseOrderRepresentation po = new PurchaseOrderRepresentation();
		po.setPlant(plantConverter.reverseTransform(plant));
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		
		CustomerRepresentation customer = new CustomerRepresentation();
		po.setCustomer(customer);
		
		ResponseEntity<PurchaseOrderRepresentation> result = restTemplate.postForEntity("http://" + host + ":" + port + "/rest/pos", po, PurchaseOrderRepresentation.class);
		
		if (result.getStatusCode().equals(HttpStatus.CONFLICT))
			throw new PlantNotAvailableException();
		
		return poConverter.transform(result.getBody());
	}
	
	public PurchaseOrderResource getPurchaseOrder(String link){
		ResponseEntity<PurchaseOrderRepresentation> result = restTemplate.getForEntity(link, PurchaseOrderRepresentation.class);
		
		return poConverter.transform(result.getBody());
	}
	
	public PurchaseOrderResource requestPurchaseOrderUpdate(PurchaseOrderResource po, Date updateDate){
		HttpHeaders requestHeaders = new HttpHeaders();
		
		PurchaseOrderRepresentation rep = poConverter.reverseTransform(po);
		rep.setEndDate(updateDate);
		
		System.out.println(updateDate.toString());
		
		HttpEntity<?> httpEntity = new HttpEntity<Object>(rep, requestHeaders);
		
		ResponseEntity<PurchaseOrderRepresentation> result = restTemplate.exchange("http://" + host + ":" + port + "/rest/pos/" + po.getPoId(), HttpMethod.PUT, httpEntity, PurchaseOrderRepresentation.class);
		
		return poConverter.transform(result.getBody());
	}
}
