package demo.services.rentit2;

import org.springframework.hateoas.Link;

import demo.integration.dto.PurchaseOrderResource;
import demo.util.ExtendedLink;

public class PurchaseOrderConverter {
	
	PlantConverter plantConverter = new PlantConverter();
	
	public PurchaseOrderResource transform(PurchaseOrderRepresentation po)
	{
		PurchaseOrderResource res = new PurchaseOrderResource();
		res.setPoId(po.getIdRes());
		res.setStartDate(po.getStartDate());
		res.setEndDate(po.getEndDate());
		res.setPlant(plantConverter.transform(po.getPlant()));
		res.setCost(po.getCost());
		
		for(Link link : po.getLinks())
		{
			res.add(link);
		}
		
		for(ExtendedLink link : po.get_links())
		{
			res.add(link);
		}
		
		return res;
	}
	
	public PurchaseOrderRepresentation reverseTransform(PurchaseOrderResource po)
	{
		PurchaseOrderRepresentation rep = new PurchaseOrderRepresentation();
		rep.setIdRes(po.getPoId());
		rep.setCustomer(new CustomerRepresentation());
		rep.setStartDate(po.getStartDate());
		rep.setEndDate(po.getEndDate());
		rep.setCost(po.getCost());
		rep.setPlant(plantConverter.reverseTransform(po.getPlant()));
		
		for(Link link : po.getLinks())
		{
			rep.add(link);
		}
		
		for(ExtendedLink link : po.get_links())
		{
			rep.add(link);
		}
		
		return rep;
	}
}
