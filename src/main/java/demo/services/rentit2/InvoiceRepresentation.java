package demo.services.rentit2;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "invoice")
@XmlType(propOrder = { "idRes", "date", "total", "purchaseOrderRef" })
public class InvoiceRepresentation {
	//Long id;
	Long idRes;
	Date date;
	Float total;
	String purchaseOrderRef;
	//InvoiceStatus invoiceStatus;
	//Boolean isRejected = false;
}
