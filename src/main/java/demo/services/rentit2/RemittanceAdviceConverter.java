package demo.services.rentit2;

import demo.models.RemittanceAdvice;

public class RemittanceAdviceConverter {

	public RemittanceAdviceRepresentation reverseTransform(RemittanceAdvice ra, Long invoiceId)
	{
		RemittanceAdviceRepresentation rep = new RemittanceAdviceRepresentation();
		rep.setIdRes(ra.getId());
		rep.setPaymentDate(ra.getDatePayed());
		rep.setInvoiceId(invoiceId);
		
		return rep;
	}
}
