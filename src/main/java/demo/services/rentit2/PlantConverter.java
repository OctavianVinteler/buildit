package demo.services.rentit2;

import java.util.ArrayList;
import java.util.List;

import demo.integration.dto.PlantResource;
import demo.util.ExtendedLink;

import org.springframework.hateoas.Link;

public class PlantConverter {
	public List<PlantResource> transform(List<PlantRepresentation> plants)
	{
		List<PlantResource> transformedPlants = new ArrayList<PlantResource>();
		
		for(PlantRepresentation plant : plants)
		{
			transformedPlants.add(transform(plant));
		}
		
		return transformedPlants;
	}
	
	public PlantResource transform(PlantRepresentation plant)
	{
		PlantResource transformedPlant = new PlantResource();
		transformedPlant.setPlantId(plant.getIdRes());
		transformedPlant.setName(plant.getName());
		transformedPlant.setDescription(plant.getDescription());
		transformedPlant.setPrice(plant.getPrice());
		
		for(Link link : plant.getLinks())
		{
			transformedPlant.add(link);
		}
		
		return transformedPlant;
	}
	
	public PlantRepresentation reverseTransform(PlantResource plant)
	{
		PlantRepresentation reversedPlant = new PlantRepresentation();
		reversedPlant.setIdRes(plant.getPlantId());
		reversedPlant.setName(plant.getName());
		reversedPlant.setDescription(plant.getDescription());
		reversedPlant.setPrice(plant.getPrice());
		
		for(Link link : plant.getLinks())
		{
			reversedPlant.add(link);
		}
		
		return reversedPlant;
	}
}
