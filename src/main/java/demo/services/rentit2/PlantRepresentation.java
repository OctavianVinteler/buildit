package demo.services.rentit2;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.util.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PlantRepresentation extends ResourceSupport  {
	Long idRes;
    String name;
    String description;
    Float price;
}
