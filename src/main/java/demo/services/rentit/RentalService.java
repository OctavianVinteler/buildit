package demo.services.rentit;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import demo.integration.dto.CustomerResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.services.PlantNotAvailableException;

@Service
public class RentalService {
	@Autowired
	RestTemplate restTemplate;
	
	/*@Value("${rentit.host}")
	String host;*/
	String host = "rentit-1102.herokuapp.com";
	//String host = "localhost";
	
	/*@Value("${rentit.port}")
	String port;*/
	String port = "80";
	//String port = "8082";
	
	public List<PlantResource> findAvailablePlants(String plantName, Date startDate, Date endDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		PlantResource[] plants = restTemplate.getForObject(
				//"http://rentit.com/rest/plants?name={name}&startDate={start}&endDate={end}",
				"http://" + host + ":" + port + "/rest/plants?name={name}&startDate={start}&endDate={end}",
				PlantResource[].class, 
				plantName,
				formatter.format(startDate),
				formatter.format(endDate));
		return Arrays.asList(plants);
	}
	
	public PlantResource getPlant(String link) {
		ResponseEntity<PlantResource> result = restTemplate.getForEntity(link, PlantResource.class);
		
		return result.getBody();
	}
	
	public PurchaseOrderResource createPurchaseOrder(PlantResource plant, Date startDate, Date endDate) throws RestClientException, PlantNotAvailableException {
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(plant);
		po.setStartDate(startDate);
		po.setEndDate(endDate);
		
		CustomerResource customer = new CustomerResource();
		customer.setName("rentit-1102");
		customer.setVatNumber("vatNumber");
		po.setCustomer(customer);
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			System.out.println(mapper.writeValueAsString(po));
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		ResponseEntity<PurchaseOrderResource> result = restTemplate.postForEntity("http://" + host + ":" + port + "/rest/pos", po, PurchaseOrderResource.class);
		
		if (result.getStatusCode().equals(HttpStatus.CONFLICT))
			throw new PlantNotAvailableException();
		
		return result.getBody();
	}
	
	public PurchaseOrderResource getPurchaseOrder(Long poID) throws RestClientException {
		//ResponseEntity<PurchaseOrderResource> result = restTemplate.getForEntity("http://rentit.com/rest/pos/" + poID, PurchaseOrderResource.class);
		ResponseEntity<PurchaseOrderResource> result = restTemplate.getForEntity("http://" + host + ":" + port + "/rest/pos/" + poID, PurchaseOrderResource.class);
		
		return result.getBody();
	}
	
	public PurchaseOrderResource getPurchaseOrder(String link){
		ResponseEntity<PurchaseOrderResource> result = restTemplate.getForEntity(link, PurchaseOrderResource.class);
		
		return result.getBody();
	}
	
	public PurchaseOrderResource rejectPurchaseOrder(Long poID) throws RestClientException {
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
		//ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://rentit.com/rest/pos/" + poID + "/accept", HttpMethod.DELETE, httpEntity, PurchaseOrderResource.class);
		ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://" + host + ":" + port + "/rest/pos/" + poID + "/accept", HttpMethod.DELETE, httpEntity, PurchaseOrderResource.class);
		
		return result.getBody();
	}
	
	public PurchaseOrderResource updatePurchaseOrder(Long poID, PurchaseOrderResource res) throws RestClientException, PlantNotAvailableException {
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(res, requestHeaders);
		//ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://rentit.com/rest/pos/" + poID, HttpMethod.PUT, httpEntity, PurchaseOrderResource.class);
		ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://" + host + ":" + port + "/rest/pos/" + poID, HttpMethod.PUT, httpEntity, PurchaseOrderResource.class);
		
		if (result.getStatusCode().equals(HttpStatus.CONFLICT))
			throw new PlantNotAvailableException();
		
		return result.getBody();
	}
	
	/*public PurchaseOrderResource closePurchaseOrder(String link) throws RestClientException
	{
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(requestHeaders);
		//ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://rentit.com/rest/pos/" + poID + "/accept", HttpMethod.DELETE, httpEntity, PurchaseOrderResource.class);
		ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://" + host + ":" + port + "/rest/pos/" + poID, HttpMethod.DELETE, httpEntity, PurchaseOrderResource.class);
		
		return result.getBody();
	}*/
	
	public PurchaseOrderResource requestPurchaseOrderUpdate(Long poID, Date updateDate){
		HttpHeaders requestHeaders = new HttpHeaders();
		HttpEntity<?> httpEntity = new HttpEntity<Object>(updateDate, requestHeaders);
		
		ResponseEntity<PurchaseOrderResource> result = restTemplate.exchange("http://" + host + ":" + port + "/rest/pos/" + poID + "/updates", HttpMethod.POST, httpEntity, PurchaseOrderResource.class);
		
		return result.getBody();
	}
}
