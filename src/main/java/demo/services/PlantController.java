package demo.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.integration.dto.PlantResource;
import demo.services.rentit.RentalService;
import demo.services.rentit2.RentalService2;
import demo.services.rentit3.RentalService3;

@RestController
@RequestMapping("/rest/plants")
public class PlantController {
	
	@Autowired
	RentalService rentit;
	
	@Autowired
	RentalService2 rentit2;
	
	@Autowired
	RentalService3 rentit3;
	
	@RequestMapping(method=RequestMethod.GET, value="")
	public List<PlantResource> queryPlantCatalog(@RequestParam(value="name", required=false) String name, 
												@RequestParam(value="startDate", required=false) Date startDate,
												@RequestParam(value="endDate", required=false) Date endDate) {
		System.out.println(">>>> Parameter: " + name);
		System.out.println(">>>> Parameter: " + startDate);
		System.out.println(">>>> Parameter: " + endDate);
		
		List<PlantResource> plants = new ArrayList<PlantResource>();
		List<PlantResource> plants2 = new ArrayList<PlantResource>();
		List<PlantResource> plants3 = new ArrayList<PlantResource>();
		
		try{
			plants = rentit.findAvailablePlants(name, startDate, endDate);
		}
		catch(Exception ex)
		{
			System.out.println("Unable to reach rentit1102");
		}
		
		try{
			plants2 = rentit2.findAvailablePlants(name, startDate, endDate);
		}
		catch(Exception ex)
		{
			System.out.println("Unable to reach rentit-team3");
		}
		
		try{
			plants3 = rentit3.findAvailablePlants(name, startDate, endDate);
		}
		catch(Exception ex)
		{
			System.out.println("Unable to reach ostap0207rentit");
			System.out.println(ex.getMessage());
		}
		
		return ListUtils.union(ListUtils.union(plants, plants2), plants3);
	}
}
