package demo.controllers;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import demo.integration.dto.PurchaseOrderResource;
import demo.integration.dto.RemittanceAdviceResourceAssembler;
import demo.models.Invoice;
import demo.models.PlantHireRequestLine;
import demo.models.PlantHireRequestExtension;
import demo.models.RemittanceAdvice;
import demo.models.repositories.InvoiceRepository;
import demo.models.repositories.PlantHireRequestLineRepository;
import demo.models.repositories.RemittanceAdviceRepository;
import demo.services.rentit2.RemittanceAdviceConverter;
import demo.services.rentit3.RemittanceAdviceConverter2;
import demo.services.rentit3.RentalService3;
import demo.util.DateHelper;
import demo.util.EmailHelper;

@Controller
@RequestMapping("/invoices")
public class InvoiceController {
	
	@Autowired
	InvoiceRepository invoiceRepo;
	
	@Autowired
	PlantHireRequestLineRepository phrRepo;
	
	@Autowired
	RemittanceAdviceRepository raRepo;
	
	RemittanceAdviceResourceAssembler raAssembler = new RemittanceAdviceResourceAssembler();
	
	RemittanceAdviceConverter raConverter = new RemittanceAdviceConverter();
	RemittanceAdviceConverter2 raConverter2 = new RemittanceAdviceConverter2();
	
	@Autowired
	RentalService3 rentit3;
	
	@Autowired
	private JavaMailSenderImpl sender;

	@RequestMapping("")
	public String getPOS(Model model){
		model.addAttribute("invoices", invoiceRepo.findAll());
		return "invoices/list";
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.POST)
	public String viewInvoice(Model model, @PathVariable Long id){
		
		String message1;
		String message2;
		String message3;
		
		Invoice invoice = invoiceRepo.getOne(id);
		
		model.addAttribute("invoice", invoice);
		
		String purchaseOrderReference = invoice.getPurchaseOrderRef();
		if(purchaseOrderReference.contains("https"))
		{
			purchaseOrderReference = purchaseOrderReference.replaceAll("https", "http");
		}
		
		List<PlantHireRequestLine> phrs = phrRepo.finderMethod(purchaseOrderReference);
		PlantHireRequestLine phr = phrs.get(0);
		
		if(phr.getExtension() != null)
		{
			Float pricePerDay = phr.getPrice() / (DateHelper.getDaysDifference(phr.getStartDate(), phr.getEndDate()) + 1);
			
			for(PlantHireRequestExtension ex : phr.getExtension())
			{
				if(ex.getEndDate().after(phr.getEndDate()))
				{
					phr.setEndDate(ex.getEndDate());
				}
			}
			
			phr.setPrice(pricePerDay * (DateHelper.getDaysDifference(phr.getStartDate(), phr.getEndDate()) + 1));
			
			message1 = "The invoice has been successfuly matched";
		}
		else
		{
			message1 = "Unsuccessful matching process for the invoice";
		}
		model.addAttribute("message1", message1);
		
		if((DateHelper.getDaysDifference(phr.getStartDate(), invoice.getStartDate()) == 0) &&
				(DateHelper.getDaysDifference(phr.getEndDate(), invoice.getEndDate()) == 0))
		{
			message2 = "The time period of the invoice is correct";
		}
		else
		{
			message2 = "Incorrect time period for the invoice";
		}
		model.addAttribute("message2", message2);
		
		if(Float.compare(phr.getPrice(), invoice.getTotal()) == 0)
		{
			message3 = "The cost of the invoice is correct";
		}
		else
		{
			message3 = "Incorrect cost for the invoice";
		}
		model.addAttribute("message3", message3);
		
		model.addAttribute("phr", phrs.get(0));
		
		return "invoices/details";
	}
	
	@RequestMapping(value="/{id}/pay", method=RequestMethod.GET)
	public String payInvoice(Model model, @PathVariable Long id){
		
		Invoice invoice = invoiceRepo.getOne(id);
		invoice.setPayed(true);
		
		invoiceRepo.saveAndFlush(invoice);
		
		RemittanceAdvice ra = new RemittanceAdvice();
		ra.setAmountPayed(invoice.getTotal());
		ra.setDatePayed(new Date());
		ra.setPurchaseOrderRef(invoice.getPurchaseOrderRef());
		
		raRepo.saveAndFlush(ra);
		
		//ra = raRepo.finderMethod(invoice.getPurchaseOrderRef());
		
		//RemittanceAdviceResource raRes = raAssembler.toResource(ra);
		
		EmailHelper email = new EmailHelper();
		try {
			if(invoice.getPurchaseOrderRef().contains("rentit-1102"))
			{
				email.sendEmail(ra, sender, "octa.rentit.esi@gmail.com", "remittanceAdvice-" + ra.getId() + ".xml");
			}
			else if(invoice.getPurchaseOrderRef().contains("rentit-team3"))
			{
				email.sendEmail(raConverter.reverseTransform(ra, invoice.getOriginalId()), sender, "rentit.ut@gmail.com", "rm_" + ra.getId() + ".xml");
			}
			else if(invoice.getPurchaseOrderRef().contains("ostap0207rentit"))
			{
				PurchaseOrderResource pos = rentit3.getPurchaseOrder(invoice.getPurchaseOrderRef());
				email.sendEmail(raConverter2.reverseTransform(ra, pos.getPoId()), sender, "esigroup2.rentit@gmail.com", "Remittance.xml");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "redirect:/invoices";
	}
}
