package demo.integration.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantHireRequestLineResourceAssembler;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.ExtensionStatus;
import demo.models.PHRStatus;
import demo.models.PlantHireRequestLine;
import demo.models.PlantHireRequestExtension;
import demo.models.repositories.PHRExtensionRepository;
import demo.models.repositories.PlantHireRequestRepository;
import demo.services.PlantHireRequestManager;
import demo.services.PlantNotAvailableException;
import demo.services.rentit.RentalService;
import demo.services.rentit2.RentalService2;
import demo.services.rentit3.RentalService3;
import demo.util.DateHelper;

@RestController
@RequestMapping("/rest/phrs")
public class PlantHireRequestLineRestController {
	@Autowired
	PlantHireRequestManager phrManager;
	PlantHireRequestLineResourceAssembler phrLineAssembler = new PlantHireRequestLineResourceAssembler();
	
	@Autowired
	PHRExtensionRepository phrExtensionRepo;
	
	@Autowired
	PlantHireRequestRepository phrRepo;
	
	@Autowired
	RentalService rentit;
	
	@Autowired
	RentalService2 rentit2;
	
	@Autowired
	RentalService3 rentit3;
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public PlantHireRequestLineResource createPHR(@RequestBody PlantHireRequestLineResource phrlResource) throws Exception {
		
		PlantHireRequestLine phrl = new PlantHireRequestLine();
		phrl.setStartDate(phrlResource.getStartDate());
		phrl.setEndDate(phrlResource.getEndDate());
		phrl.setPlantRef(phrlResource.getPlant().getLink("self").getHref());
		phrl.setPrice(phrlResource.getPlant().getPrice() * (DateHelper.getDaysDifference(phrlResource.getStartDate(), phrlResource.getEndDate()) + 1));
		phrManager.createPlantHireRequest(phrl);
		
		return phrLineAssembler.toResource(phrl);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<PlantHireRequestLineResource> getAllPlantHireRequests(){
		List<PlantHireRequestLine> phrs = phrManager.getAllPlantHireRequests();
		return phrLineAssembler.toResource(phrs);
	}
	
	@RequestMapping(value="{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource getPlantHireRequest(@PathVariable("id") Long id) throws Exception{
		PlantHireRequestLine phr = phrManager.getPlantHireRequest(id);
		
		if(phr == null) {
			throw new ResourceNotFoundException();
		}
		
		return phrLineAssembler.toResource(phr);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource updatePHR(@RequestBody PlantHireRequestLineResource phrResource, @PathVariable("id") Long id) throws Exception{

		PlantHireRequestLine phr = phrManager.getPlantHireRequest(id);
		
		/*if(phrResource.getStartDate().after(phrResource.getEndDate()))
		{
			throw new NegativeTimeIntervalException(poResource.getStartDate(), poResource.getEndDate());
		}*/
		
		Float pricePerDay = phr.getPrice() / (DateHelper.getDaysDifference(phr.getStartDate(), phr.getEndDate()) + 1);
		
		phr.setStartDate(phrResource.getStartDate());
		phr.setEndDate(phrResource.getEndDate());
		phr.setPrice(pricePerDay * (DateHelper.getDaysDifference(phrResource.getStartDate(), phrResource.getEndDate()) + 1));
		phr.setStatus(PHRStatus.PENDING);
		
		phrManager.updatePlantHireRequest(phr);
		
		return phrLineAssembler.toResource(phr);
	}
	
	@RequestMapping(value="/{id}/approve", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public PlantHireRequestLineResource approvePlantHireRequest(@PathVariable("id") Long id) throws RestClientException, PlantNotAvailableException{
		
		PlantHireRequestLineResource res = phrLineAssembler.toResource(phrManager.getPlantHireRequest(id));
		PurchaseOrderResource poRes = null;
		
		res.setPurchaseOrder(new PurchaseOrderResource());
		res.getPurchaseOrder().setStartDate(res.getStartDate());
		res.getPurchaseOrder().setEndDate(res.getEndDate());
		res.getPurchaseOrder().setCost(res.getCost());
		if(res.getPlant().getLink("self").getHref().contains("rentit-1102"))
		{
			res.getPurchaseOrder().setPlant(rentit.getPlant(res.getPlant().getLink("self").getHref()));
			poRes = rentit.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
		}
		else if(res.getPlant().getLink("self").getHref().contains("rentit-team3"))
		{
			res.getPurchaseOrder().setPlant(rentit2.getPlant(res.getPlant().getLink("self").getHref()));
			poRes = rentit2.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
		}
		else if(res.getPlant().getLink("self").getHref().contains("ostap0207rentit"))
		{
			res.getPurchaseOrder().setPlant(rentit3.getPlant(res.getPlant().getLink("self").getHref()));
			poRes = rentit3.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
		}
		
		//PurchaseOrderResource poRes = rentit.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
		
		phrManager.approvePlantHireRequest(id, poRes.getLink("self").getHref());
		
		res = phrLineAssembler.toResource(phrManager.getPlantHireRequest(id));
		res.setPurchaseOrder(poRes);
		
		return res;
	}
	
	@RequestMapping(value="/{id}/approve", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource rejectPlantHireRequest(@PathVariable("id") Long id){
		
		phrManager.rejectPlantHireRequest(id);
		
		return phrLineAssembler.toResource(phrManager.getPlantHireRequest(id));
	}
	
	@RequestMapping(value="/{id}/return", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource endPHR(@PathVariable("id") Long id){
		
		phrManager.endPlantHireRequest(id);
		
		PlantHireRequestLine phr = phrManager.getPlantHireRequest(id);
		
		/*if(phr.getPurchaseOrderRef().contains(""))
		{		
			rentit.closePurchaseOrder(phr.getPurchaseOrderRef());
		}*/
		
		return phrLineAssembler.toResource(phrManager.getPlantHireRequest(id));
	}
	
	@RequestMapping(value="/{id}/extensions", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource requestPHRExtension(@PathVariable("id") Long id, @RequestBody Date extensionDate){

		PlantHireRequestLine phr = phrManager.getPlantHireRequest(id);
		
		PlantHireRequestExtension extension = new PlantHireRequestExtension();
		extension.setStatus(ExtensionStatus.PENDING);
		extension.setEndDate(extensionDate);
		
		phrExtensionRepo.saveAndFlush(extension);
		
		if(phr.getExtension() == null)
		{
			phr.setExtension(new ArrayList<PlantHireRequestExtension>());
		}
		phr.getExtension().add(extension);
		
		phrManager.requestExtensionPlantHireRequest(id);
		
		return phrLineAssembler.toResource(phrManager.getPlantHireRequest(id));
	}
	
	@RequestMapping(value="/{phrid}/extensions/{exid}/approve", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource rejectPHRExtension(@PathVariable("phrid") Long phrid, @PathVariable("exid") Long exid){
		PlantHireRequestLine phr = phrManager.getPlantHireRequest(phrid);
		
		PlantHireRequestExtension extension = null;
		
		for(PlantHireRequestExtension ex : phr.getExtension())
		{
			if(ex.getId().longValue() == exid.longValue())
			{
				extension = ex;
				break;
			}
		}
		
		if(extension != null)
		{
			extension.setStatus(ExtensionStatus.REJECTED);
			phrExtensionRepo.saveAndFlush(extension);
		}
		
		phrManager.rejectExtensionPlantHireRequest(phrid, exid);
		
		return phrLineAssembler.toResource(phrManager.getPlantHireRequest(phrid));
	}
	
	@RequestMapping(value="/{phrid}/extensions/{exid}/approve", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource approvePHRExtension(@PathVariable("phrid") Long phrid, @PathVariable("exid") Long exid){
		PlantHireRequestLine phr = phrManager.getPlantHireRequest(phrid);
		
		PlantHireRequestExtension extension = null;
		
		for(PlantHireRequestExtension ex : phr.getExtension())
		{
			if(ex.getId().longValue() == exid.longValue())
			{
				extension = ex;
				break;
			}
		}
		
		if(extension != null)
		{
			PurchaseOrderResource poRes = null;
			if(phr.getPurchaseOrderRef().contains("rentit-1102"))
			{
				poRes = rentit.getPurchaseOrder(phr.getPurchaseOrderRef());
				poRes = rentit.requestPurchaseOrderUpdate(poRes.getPoId(), extension.getEndDate());
			}
			else if(phr.getPurchaseOrderRef().contains("rentit-team3"))
			{
				poRes = rentit2.getPurchaseOrder(phr.getPurchaseOrderRef());
				poRes = rentit2.requestPurchaseOrderUpdate(poRes, extension.getEndDate());
			}
			else if(phr.getPurchaseOrderRef().contains("ostap0207rentit"))
			{
				poRes = rentit3.getPurchaseOrder(phr.getPurchaseOrderRef());
				poRes = rentit3.requestPurchaseOrderUpdate(poRes, extension.getEndDate());
			}
			
			extension.setStatus(ExtensionStatus.APPROVED);
			phrExtensionRepo.saveAndFlush(extension);
		}
		
		phrManager.approveExtensionPlantHireRequest(phrid, exid);
		
		return phrLineAssembler.toResource(phrManager.getPlantHireRequest(phrid));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestLineResource cancelPHR(@PathVariable("id") Long id){

		phrManager.cancelPlantHireRequest(id);
		
		return phrLineAssembler.toResource(phrManager.getPlantHireRequest(id));
	}

}
