package demo.integration.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantHireRequestLineResourceAssembler;
import demo.integration.dto.PlantHireRequestResource;
import demo.integration.dto.PlantHireRequestResourceAssembler;
import demo.integration.dto.PurchaseOrderResource;
import demo.models.PHRStatus;
import demo.models.PlantHireRequest;
import demo.models.PlantHireRequestLine;
import demo.models.repositories.PlantHireRequestRepository;
import demo.services.PlantHireRequestManager;
import demo.services.PlantNotAvailableException;
import demo.services.rentit.RentalService;
import demo.services.rentit2.RentalService2;
import demo.services.rentit3.RentalService3;
import demo.util.DateHelper;

@RestController
@RequestMapping("/rest/prs")
public class PlantHireRequestRestController {

	@Autowired
	PlantHireRequestRepository phrRepo;
	
	@Autowired
	PlantHireRequestManager phrManager;
	
	PlantHireRequestResourceAssembler phrAssembler = new PlantHireRequestResourceAssembler();
	PlantHireRequestLineResourceAssembler phrLineAssembler = new PlantHireRequestLineResourceAssembler();
	
	@Autowired
	RentalService rentit;
	
	@Autowired
	RentalService2 rentit2;
	
	@Autowired
	RentalService3 rentit3;
	
	@RequestMapping(method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<PlantHireRequestResource> getAllPlantHireRequests(){
		List<PlantHireRequest> phrs = phrRepo.findAll();
		
		for(PlantHireRequest phr : phrs)
		{
			Float totalCost = 0f;
			
			for(PlantHireRequestLine line : phr.getLines())
			{
				totalCost += line.getPrice();
			}
			
			if(phr.getTotalPrice().floatValue() != totalCost.floatValue())
			{
				phr.setTotalPrice(totalCost);
				phrRepo.saveAndFlush(phr);
			}
		}
		
		return phrAssembler.toResource(phrs);
	}
	
	@RequestMapping(value="{id}")
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestResource getPlantHireRequest(@PathVariable("id") Long id) throws Exception{
		PlantHireRequest phr = phrRepo.findOne(id);
		
		if(phr == null) {
			throw new ResourceNotFoundException();
		}
		
		Float totalCost = 0f;
		
		for(PlantHireRequestLine line : phr.getLines())
		{
			totalCost += line.getPrice();
		}
		
		if(phr.getTotalPrice().floatValue() != totalCost.floatValue())
		{
			phr.setTotalPrice(totalCost);
			phrRepo.saveAndFlush(phr);
		}
		
		return phrAssembler.toResource(phr);
	}
	
	@RequestMapping(value="{id}/lines", method=RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public List<PlantHireRequestLineResource> getAllLinesForPlantHireRequest(@PathVariable("id") Long id) throws Exception{
		PlantHireRequest phr = phrRepo.findOne(id);
		
		if(phr == null) {
			throw new ResourceNotFoundException();
		}
		
		return phrLineAssembler.toResource(phr.getLines());
	}
	
	@RequestMapping(value="{id}/lines", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.OK)
	public PlantHireRequestResource addNewPlantHireRequestLine(@PathVariable("id") Long id, @RequestBody PlantHireRequestLineResource phrlResource) throws Exception{
		PlantHireRequest phr = phrRepo.findOne(id);
		
		if(phr == null) {
			throw new ResourceNotFoundException();
		}
		
		PlantHireRequestLine phrl = new PlantHireRequestLine();
		phrl.setStartDate(phrlResource.getStartDate());
		phrl.setEndDate(phrlResource.getEndDate());
		phrl.setPlantRef(phrlResource.getPlant().getLink("self").getHref());
		phrl.setPrice(phrlResource.getPlant().getPrice() * (DateHelper.getDaysDifference(phrlResource.getStartDate(), phrlResource.getEndDate()) + 1));
		phrManager.createPlantHireRequest(phrl);
		
		phr.setLinesNumber(phr.getLinesNumber() + 1);
		phr.setTotalPrice(phr.getTotalPrice() + phrl.getPrice());
		phr.getLines().add(phrl);
		
		phrRepo.saveAndFlush(phr);
		
		return phrAssembler.toResource(phr);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.CREATED)
	public PlantHireRequestResource createPHR(@RequestBody PlantHireRequestLineResource phrlResource) throws Exception {
		
		PlantHireRequest phr = new PlantHireRequest();
		phr.setLines(new ArrayList<PlantHireRequestLine>());
		
		PlantHireRequestLine phrl = new PlantHireRequestLine();
		phrl.setStartDate(phrlResource.getStartDate());
		phrl.setEndDate(phrlResource.getEndDate());
		phrl.setPlantRef(phrlResource.getPlant().getLink("self").getHref());
		phrl.setPrice(phrlResource.getPlant().getPrice() * (DateHelper.getDaysDifference(phrlResource.getStartDate(), phrlResource.getEndDate()) + 1));
		phrManager.createPlantHireRequest(phrl);
		
		phr.setLinesNumber(1);
		phr.setTotalPrice(phrl.getPrice());
		phr.getLines().add(phrl);
		
		phrRepo.saveAndFlush(phr);
		
		return phrAssembler.toResource(phr);
	}
	
	@RequestMapping(value="/{id}/approve", method=RequestMethod.POST)
	@ResponseStatus(HttpStatus.ACCEPTED)
	public PlantHireRequestResource approveLines(@PathVariable("id") Long id) throws RestClientException, PlantNotAvailableException{
		
		PlantHireRequest phr = phrRepo.findOne(id);
		
		for(PlantHireRequestLine line : phr.getLines())
		{
			if(line.getStatus() != PHRStatus.PENDING)
			{
				continue;
			}
			
			PlantHireRequestLineResource res = phrLineAssembler.toResource(line);
			PurchaseOrderResource poRes = null;
			
			res.setPurchaseOrder(new PurchaseOrderResource());
			res.getPurchaseOrder().setStartDate(res.getStartDate());
			res.getPurchaseOrder().setEndDate(res.getEndDate());
			res.getPurchaseOrder().setCost(res.getCost());
			if(res.getPlant().getLink("self").getHref().contains("rentit-1102"))
			{
				res.getPurchaseOrder().setPlant(rentit.getPlant(res.getPlant().getLink("self").getHref()));
				poRes = rentit.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
			}
			else if(res.getPlant().getLink("self").getHref().contains("rentit-team3"))
			{
				res.getPurchaseOrder().setPlant(rentit2.getPlant(res.getPlant().getLink("self").getHref()));
				poRes = rentit2.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
			}
			else if(res.getPlant().getLink("self").getHref().contains("ostap0207rentit"))
			{
				res.getPurchaseOrder().setPlant(rentit3.getPlant(res.getPlant().getLink("self").getHref()));
				poRes = rentit3.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
			}
			
			//PurchaseOrderResource poRes = rentit.createPurchaseOrder(res.getPurchaseOrder().getPlant(), res.getPurchaseOrder().getStartDate(), res.getPurchaseOrder().getEndDate());
			
			phrManager.approvePlantHireRequest(line.getId(), poRes.getLink("self").getHref());
		}
		
		return phrAssembler.toResource(phr);
	}
}
