package demo.integration.dto;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.models.ExtensionStatus;
import demo.util.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PHRExtensionResource extends ResourceSupport{
	@Id
    @GeneratedValue
    Long extensionId;
	
	@Temporal(TemporalType.DATE)
    Date endDate;
	
	@Enumerated(EnumType.STRING)
	ExtensionStatus status;
}
