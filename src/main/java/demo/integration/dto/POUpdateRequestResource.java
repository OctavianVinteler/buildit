package demo.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="poUpdateRequest")
@JsonIgnoreProperties(ignoreUnknown = true)
public class POUpdateRequestResource extends ResourceSupport {

	Long updateId;
	Date endDate;
}
