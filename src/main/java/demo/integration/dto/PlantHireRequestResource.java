package demo.integration.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.util.ResourceSupport;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PlantHireRequestResource extends ResourceSupport {
	Long requestID;

	List<PlantHireRequestLineResource> lines;
	Float totalPrice;
	int linesNumber;
}
