package demo.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@XmlRootElement(name="invoice")
public class InvoiceResource  extends ResourceSupport {

	Long invoiceId;
	
	Boolean payed;
	Date startDate;
	Date endDate;
	Date dueDate;
	Float totalCost;
	String purchaseOrderRef;
	Boolean latePayment;
}
