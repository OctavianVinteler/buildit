package demo.integration.dto;

import java.util.ArrayList;
import java.util.List;

import demo.models.PlantHireRequestExtension;

public class PHRExtensionResourceAssembler {

	public PHRExtensionResource toResource(PlantHireRequestExtension extension){
		PHRExtensionResource res = new PHRExtensionResource();
		
		res.setExtensionId(extension.getId());
		res.setEndDate(extension.getEndDate());
		res.setStatus(extension.getStatus());
		return res;
	}
	
	public List<PHRExtensionResource> toResource(List<PlantHireRequestExtension> extensions){
		if(extensions == null)
		{
			return null;
		}
		
		List<PHRExtensionResource> ress = new ArrayList<PHRExtensionResource>();
		
		for(PlantHireRequestExtension extension : extensions){
			ress.add(toResource(extension));
		}
		
		return ress;
	}
}
