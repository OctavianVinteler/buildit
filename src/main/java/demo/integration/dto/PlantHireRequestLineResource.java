package demo.integration.dto;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
public class PlantHireRequestLineResource extends ResourceSupport {
    Long requestID;
	
	PlantResource plant;
    PurchaseOrderResource purchaseOrder;
    
    @DateTimeFormat(iso=ISO.DATE)
    Date startDate;
    @DateTimeFormat(iso=ISO.DATE)
    Date endDate;
    Float cost;
    List<PHRExtensionResource> extensions;
}