package demo.integration.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@XmlRootElement(name="remittanceAdvice")
public class RemittanceAdviceResource extends ResourceSupport {

	Long remittanceAdviceId;
	Date datePayed;
	Float amountPayed;
	String purchaseOrderRef;
}
