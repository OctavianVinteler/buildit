package demo.integration.dto;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import demo.util.ResourceSupport;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(Include.NON_NULL)
@XmlRootElement(name="purchaseOrder")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseOrderResource extends ResourceSupport {
	Long poId;
	Date startDate;
	Date endDate;
	PlantResource plant;
	CustomerResource customer;
	List<POUpdateRequestResource> updates;
	Float cost;
	InvoiceResource invoice;
	RemittanceAdviceResource remittanceAdvice;
}