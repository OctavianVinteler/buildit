package demo.integration.dto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.integration.rest.PlantHireRequestLineRestController;
import demo.models.ExtensionStatus;
import demo.models.PlantHireRequestLine;
import demo.models.PlantHireRequestExtension;
import demo.util.DateHelper;
import demo.util.ExtendedLink;

public class PlantHireRequestLineResourceAssembler extends ResourceAssemblerSupport<PlantHireRequestLine, PlantHireRequestLineResource> {

	private PHRExtensionResourceAssembler extensionAssembler = new PHRExtensionResourceAssembler();
	
	public PlantHireRequestLineResourceAssembler() {
		super(PlantHireRequestLineRestController.class, PlantHireRequestLineResource.class);
	}

	@Override
	public PlantHireRequestLineResource toResource(PlantHireRequestLine phr) {
		PlantHireRequestLineResource res = createResourceWithId(phr.getId(), phr);
		res.setRequestID(phr.getId());
		res.setStartDate(phr.getStartDate());
		res.setEndDate(phr.getEndDate());
		res.setCost(phr.getPrice());
		
		if (phr.getPlantRef() != null) {
			PlantResource plantRes = new PlantResource();
			plantRes.add(new Link(phr.getPlantRef()));
			res.setPlant(plantRes);
		}
		if (phr.getPurchaseOrderRef() != null) {
			PurchaseOrderResource poRes = new PurchaseOrderResource();
			poRes.add(new Link(phr.getPurchaseOrderRef()));
			res.setPurchaseOrder(poRes);
		}
		
		Float pricePerDay = phr.getPrice() / (DateHelper.getDaysDifference(phr.getStartDate(), phr.getEndDate()) + 1);
		
		if(phr.getExtension() != null)
		{
			res.setExtensions(extensionAssembler.toResource(phr.getExtension()));
			
			for(PlantHireRequestExtension ext : phr.getExtension())
			{
				if(ext.getStatus() == ExtensionStatus.APPROVED && ext.getEndDate().after(res.getEndDate()))
				{
					res.setEndDate(ext.getEndDate());
					res.setCost(pricePerDay * (DateHelper.getDaysDifference(res.getStartDate(), res.getEndDate()) + 1));
				}
			}
		}

		try {
			switch (phr.getStatus()) {
			case APPROVED:
				//res.add(linkTo(methodOn(PlantHireRequestRestController.class).requestPHRExtension(phr.getId())).withRel("requestExtension"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).requestPHRExtension(phr.getId(), DateHelper.addDays(phr.getEndDate(), 7))).toString(), "requestExtension", "POST"));
				//res.add(linkTo(methodOn(PlantHireRequestRestController.class).endPHR(phr.getId())).withRel("endPlantHireRequest"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).endPHR(phr.getId())).toString(), "endPlantHireRequest", "DELETE"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).cancelPHR(phr.getId())).toString(), "cancel", "DELETE"));
				break;
			case PENDING:
				//res.add(linkTo(methodOn(PlantHireRequestRestController.class).approvePlantHireRequest(phr.getId())).withRel("approve"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).approvePlantHireRequest(phr.getId())).toString(), "approve", "POST"));
				//res.add(linkTo(methodOn(PlantHireRequestRestController.class).rejectPlantHireRequest(phr.getId())).withRel("reject"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).rejectPlantHireRequest(phr.getId())).toString(), "reject", "DELETE"));
				//res.add(linkTo(methodOn(PlantHireRequestRestController.class).updatePHR(res, phr.getId())).withRel("update"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).updatePHR(res, phr.getId())).toString(), "update", "PUT"));
				//res.add(linkTo(methodOn(PlantHireRequestRestController.class).cancelPHR(phr.getId())).withRel("cancel"));
				res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).cancelPHR(phr.getId())).toString(), "cancel", "DELETE"));
				break;
			case REJECTED:
				break;
			case ENDED:
				break;
			case EXTENSION_PENDING:
				PlantHireRequestExtension extension = null;
		    	
		    	for(PlantHireRequestExtension ext : phr.getExtension())
		    	{
		    		if(ext.getStatus() == ExtensionStatus.PENDING)
		    		{
		    			extension = ext;
		    		}
		    	}
				
		    	if(extension != null)
		    	{
		    		//res.add(linkTo(methodOn(PlantHireRequestRestController.class).approvePHRExtension(phr.getId(), extension.getId())).withRel("approveExtension"));
		    		res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).approvePHRExtension(phr.getId(), extension.getId())).toString(), "approveExtension", "POST"));
		    		//res.add(linkTo(methodOn(PlantHireRequestRestController.class).rejectPHRExtension(phr.getId(), extension.getId())).withRel("rejectExtension"));
		    		res.add(new ExtendedLink(linkTo(methodOn(PlantHireRequestLineRestController.class).rejectPHRExtension(phr.getId(), extension.getId())).toString(), "rejectExtension", "DELETE"));
		    	}
				break;
			case CANCELLED:
				break;
			}
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		return res;
	}
	
	public List<PlantHireRequestLineResource> toResource(List<PlantHireRequestLine> phrs) {
		List<PlantHireRequestLineResource> ress = new ArrayList<PlantHireRequestLineResource>();
		
		for(PlantHireRequestLine phr: phrs)
		{
			ress.add(toResource(phr));
		}
		
		return ress;
	}

}
