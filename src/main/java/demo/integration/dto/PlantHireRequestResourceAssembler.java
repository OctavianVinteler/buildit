package demo.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.integration.rest.PlantHireRequestLineRestController;
import demo.models.PlantHireRequest;

public class PlantHireRequestResourceAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestResource> {

	private PlantHireRequestLineResourceAssembler lineAssembler = new PlantHireRequestLineResourceAssembler();
	
	public PlantHireRequestResourceAssembler() {
		super(PlantHireRequestLineRestController.class, PlantHireRequestResource.class);
	}
	
	@Override
	public PlantHireRequestResource toResource(PlantHireRequest phr) {
		PlantHireRequestResource res = createResourceWithId(phr.getId(), phr);
		res.setRequestID(phr.getId());
		
		if(phr.getLines() != null)
		{
			res.setLines(lineAssembler.toResource(phr.getLines()));
		}
		
		res.setLinesNumber(phr.getLinesNumber());
		res.setTotalPrice(phr.getTotalPrice());
		
		return res;
	}
	
	public List<PlantHireRequestResource> toResource(List<PlantHireRequest> phrs) {
		List<PlantHireRequestResource> ress = new ArrayList<PlantHireRequestResource>();
		
		for(PlantHireRequest phr: phrs)
		{
			ress.add(toResource(phr));
		}
		
		return ress;
	}
}
