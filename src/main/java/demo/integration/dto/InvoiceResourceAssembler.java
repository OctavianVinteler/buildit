package demo.integration.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import demo.integration.rest.PlantHireRequestLineRestController;
import demo.models.Invoice;

public class InvoiceResourceAssembler extends ResourceAssemblerSupport<Invoice, InvoiceResource> {

	public InvoiceResourceAssembler() {
	    super(PlantHireRequestLineRestController.class, InvoiceResource.class);
	}
	
	public InvoiceResource toResource(Invoice inv){
		InvoiceResource res = createResourceWithId(inv.getId(), inv);
		res.setInvoiceId(inv.getId());
		res.setPayed(inv.getPayed());
		res.setStartDate(inv.getStartDate());
		res.setEndDate(inv.getEndDate());
		res.setDueDate(inv.getDueDate());
		res.setTotalCost(inv.getTotal());
		res.setPurchaseOrderRef(inv.getPurchaseOrderRef());
		res.setLatePayment(inv.getLatePayment());
		
		return res;
	}
	
	public List<InvoiceResource> toResource(List<Invoice> invs){
		List<InvoiceResource> ress = new ArrayList<InvoiceResource>();
		
		for(Invoice inv: invs)
		{
			ress.add(toResource(inv));
		}
		
		return ress;
	}
}
