package demo.integration.email;

import java.io.StringReader;
import java.util.Date;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;

import demo.integration.dto.PurchaseOrderResource;
import demo.models.Invoice;
import demo.models.RemittanceAdvice;
import demo.models.repositories.InvoiceRepository;
import demo.models.repositories.RemittanceAdviceRepository;
import demo.services.rentit2.InvoiceConverter;
import demo.services.rentit2.InvoiceRepresentation;
import demo.services.rentit2.RemittanceAdviceConverter;
import demo.services.rentit3.InvoiceConverter2;
import demo.services.rentit3.InvoiceRepresentation2;
import demo.services.rentit3.RemittanceAdviceConverter2;
import demo.services.rentit3.RentalService3;
import demo.util.EmailHelper;

@Service
public class NormalTrackInvoice {

	@Autowired
	InvoiceRepository invoiceRepo;
	
	@Autowired
	RemittanceAdviceRepository raRepo;
	
	@Autowired
	private JavaMailSenderImpl sender;
	
	@Autowired
	RentalService3 rentit3;
	
	InvoiceConverter invoiceConverter = new InvoiceConverter();
	InvoiceConverter2 invoiceConverter2 = new InvoiceConverter2();
	RemittanceAdviceConverter raConverter = new RemittanceAdviceConverter();
	RemittanceAdviceConverter2 raConverter2 = new RemittanceAdviceConverter2();
	
	public void processInvoice(String invoice) throws Exception {

		JAXBContext jaxbContext;
        Unmarshaller unmarshaller;
        StringReader reader;
        Invoice inv = null;
        
        if(invoice.contains("rentit-1102"))
        {
        	jaxbContext = JAXBContext.newInstance(Invoice.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            reader = new StringReader(invoice);
            inv = (Invoice) unmarshaller.unmarshal(reader);
        }
        else if(invoice.contains("rentit-team3"))
        {
        	jaxbContext = JAXBContext.newInstance(InvoiceRepresentation.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            reader = new StringReader(invoice);
            inv = invoiceConverter.transform((InvoiceRepresentation) unmarshaller.unmarshal(reader));
        }
        else if(invoice.contains("ostap0207rentit"))
        {
        	jaxbContext = JAXBContext.newInstance(InvoiceRepresentation2.class);
            unmarshaller = jaxbContext.createUnmarshaller();
            reader = new StringReader(invoice);
            inv = invoiceConverter2.transform((InvoiceRepresentation2) unmarshaller.unmarshal(reader));
        }
		
		/*JAXBContext jaxbContext = JAXBContext.newInstance(Invoice.class);
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        StringReader reader = new StringReader(invoice);
        Invoice inv = (Invoice) unmarshaller.unmarshal(reader);*/
        
        if(invoiceRepo.finderMethod(inv.getPurchaseOrderRef()) != null)
        {
        	if(raRepo.finderMethod(inv.getPurchaseOrderRef()) != null)
        	{
        		RemittanceAdvice ra = raRepo.finderMethod(inv.getPurchaseOrderRef());
        		
        		EmailHelper email = new EmailHelper();
        		try {
        			if(inv.getPurchaseOrderRef().contains("rentit-1102"))
        			{
        				email.sendEmail(ra, sender, "octa.rentit.esi@gmail.com", "remittanceAdvice-" + ra.getId() + ".xml");
        			}
        			else if(inv.getPurchaseOrderRef().contains("rentit-team3"))
        			{
        				email.sendEmail(raConverter.reverseTransform(ra, inv.getOriginalId()), sender, "rentit.ut@gmail.com", "rm_" + ra.getId() + ".xml");
        			}
        			else if(inv.getPurchaseOrderRef().contains("ostap0207rentit"))
        			{
        				PurchaseOrderResource pos = rentit3.getPurchaseOrder(inv.getPurchaseOrderRef());
        				email.sendEmail(raConverter2.reverseTransform(ra, pos.getPoId()), sender, "esigroup2.rentit@gmail.com", "Remittance.xml");
        			}
        		} catch (Exception e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}
        	}
        	else
        	{
        		Invoice actualInvoice = invoiceRepo.finderMethod(inv.getPurchaseOrderRef());
        		
        		/*if(new Date().after(actualInvoice.getDueDate()))
        		{
        			actualInvoice.setLatePayment(true);
        			invoiceRepo.saveAndFlush(actualInvoice);
        		}*/
        		
        		actualInvoice.setLatePayment(true);
    			invoiceRepo.saveAndFlush(actualInvoice);
        	}
        }
        else
        {
        	invoiceRepo.saveAndFlush(inv);
        }
    }
}
