--insert into plant (plant_id, name, description, price) values (1, 'Excavator', '1.5 Tonne Mini excavator', 150.0)
--insert into plant (plant_id, name, description, price) values (2, 'Excavator', '3 Tonne Mini excavator', 200.0)
--insert into plant (id, name, description, price) values (3, 'Excavator', '5 Tonne Midi excavator', 250.0)
--insert into plant (id, name, description, price) values (4, 'Excavator', '8 Tonne Midi excavator', 300.0)
--insert into plant (id, name, description, price) values (5, 'Excavator', '15 Tonne Large excavator', 400.0)
--insert into plant (id, name, description, price) values (6, 'Excavator', '20 Tonne Large excavator', 450.0)

--insert into plant_hire_request (id, price, end_date, start_date, status, plant_ref, purchase_order_ref) values (1, 600.0, '2014-11-23', '2014-11-20', 'APPROVED', 'http://localhost:8082/rest/plants/1', 'http://localhost:8082/rest/pos/1')
--insert into plant_hire_request (id, price, end_date, start_date, status, plant_ref, purchase_order_ref) values (2, 800.0, '2014-11-29', '2014-11-26', 'APPROVED', 'http://localhost:8082/rest/plants/2', 'http://localhost:8082/rest/pos/2')
