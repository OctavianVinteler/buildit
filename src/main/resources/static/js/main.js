var app = angular.module('project', ["ngRoute", "ngCookies"]);

app.config(function($routeProvider, $locationProvider, $httpProvider) {
	$routeProvider
	    .when('/plants', {
	      controller:'PlantsController',
	      templateUrl:'views/plants/list.html',
	      resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN", "ROLE_WORK_ENGINEER"]);
	            }
	        }
	    })
	    .when('/plants/:id', {
	      controller:'GetPlantController',
	      templateUrl:'views/plants/plant.html',
	      resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN", "ROLE_WORK_ENGINEER"]);
	            }
	        }
	    })
	    .when('/phrs/create', {
	      controller:'CreatePHRController',
	      templateUrl:'views/phrs/create.html',
	      resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN"]);
	            }
	        }
	    })
	    .when('/phrs/:id/addPlant', {
	      controller:'AddPlantController',
	      templateUrl:'views/phrs/addPlant.html',
	      resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN"]);
	            }
	        }
	    })
	    .when('/phrs', {
		    controller: 'PHRController',
		    templateUrl: 'views/phrs/phrList.html',
	    	resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN", "ROLE_WORK_ENGINEER"]);
	            }
	        }
		})
		.when('/phrs/:id/lines', {
			controller: 'PHRLinesByPHRController',
		    templateUrl: 'views/phrs/list.html',
	    	resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN", "ROLE_WORK_ENGINEER"]);
	            }
	        }
		})
		.when('/phrs/update/:id', {
			controller: 'UpdatePHRController',
			templateUrl: 'views/phrs/update.html',
			resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_WORK_ENGINEER", "ROLE_ADMIN"]);
	            }
	        }
		})
		.when('/phrs/extend/:id', {
			controller: 'ExtendPHRController',
			templateUrl: 'views/phrs/extend.html',
			resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN"]);
	            }
	        }
		})
		.when('/phrs/cancel/:id', {
			controller: 'CancelPHRController',
			templateUrl: 'views/phrs/cancel.html',
			resolve: {
	            permission: function(RoleBasedAccessService, $route) {
	                return RoleBasedAccessService.permissionCheck(["ROLE_SITE_ENGINEER", "ROLE_ADMIN"]);
	            }
	        }
		})
		.when('/login', {
	        controller:'LoginController',
	        templateUrl:'views/login.html'
		})
	    .otherwise({
	      redirectTo:'/login'
	    });
	
	$httpProvider.interceptors
	    .push(function($q, $rootScope, $location, $window) {
	        return {
	            'request' : function(config) {
	                var isRestCall = config.url.indexOf('/rest') == 0;
	                if (isRestCall) {
	                    config.headers['Authorization'] = 'Basic ' + $rootScope.code;
	                }
	                return config || $q.when(config);
	            },
	            'responseError' : function(rejection) {
	                var status = rejection.status;
	                var config = rejection.config;
	                var method = config.method;
	                var url = config.url;
	
	                if (status == 401) {
	                    $window.history.back();
	                } else {
	                    $rootScope.error = method + " on " + url
	                            + " failed with status " + status;
	                }
	
	                return $q.reject(rejection);
	            }
	        };
	    });
	})
	
app.run(function($rootScope, $location, $cookieStore) {
    var user = $cookieStore.get("user");
    var code = $cookieStore.get("code");
    if (user !== undefined && code !== undefined) {
        $rootScope.user = user;
        $rootScope.code = code;
    }

    $rootScope.logout = function() {
        delete $rootScope.user;
        delete $rootScope.code;

        $cookieStore.remove("user");
        $cookieStore.remove("code");
        $cookieStore.remove("userRoles");
        $location.path("/login");
    };
});
	
app.service("RoleBasedAccessService", function($rootScope, $location, $q,
        $cookieStore) {
    return {
        permissionCheck : function(roleCollection) {
            var deferred = $q.defer();
            var userRoles = $cookieStore.get("userRoles");

            console.log("-------------");
            console.log("role collection");
            console.log(roleCollection);
            console.log("user roles");
            console.log(userRoles);
            console.log("-------------");
            
            if(userRoles == undefined)
        	{
            	$location.path("/login");
            	$location.reload();
        	}

            var matchingRoles = userRoles.filter(function(role) {
                return roleCollection.indexOf(role) != -1;
            });
            if (userRoles !== undefined && matchingRoles.length > 0)
                deferred.resolve();
            else {
                $location.path("/login");
                $rootScope.$on("$locationChangeSuccess",
                        function(next, current) {
                            deferred.resolve();
                        });
            }
            return deferred.promise;
        }
    };
});