var app = angular.module('project');

app.controller('PlantsController', function($scope, $http) {
	$scope.plants = [];
	$http.get('/rest/plants').success(function(data, status, headers, config) {
		console.log(JSON.stringify(data));
		$scope.plants = data;
	});
	/*$scope.plants = $scope.plants = [{
	        "id": 10002,
	        "name": "Excavator",
	        "description": "3 Tonne Mini excavator",
	        "price": 200,
	        "links":[
	            {"rel":"self","href":"http://localhost/rest/plants/10002","variableNames":[],"variables":[],"templated":false}
	        ]
	    },
	    {
	        "id": 10004,
	        "name": "Excavator",
	        "description": "8 Tonne Midi excavator",
	        "price": 300,
	        "links":[
	            {"rel":"self","href":"http://localhost/rest/plants/10004","variableNames":[],"variables":[],"templated":false}
	        ]
	    },
	    {
	        "id": 10005,
	        "name": "Excavator",
	        "description": "15 Tonne Large excavator",
	        "price": 400,
	        "links":[
	            {"rel":"self","href":"http://localhost/rest/plants/10005","variableNames":[],"variables":[],"templated":false}
	        ]
	    }
	];*/
	
});

app.controller('GetPlantController', function($scope, $http) {
	
});

app.controller('CreatePHRController', function($scope, $http, $location){
	$scope.name = '';
	$scope.startDate = new Date();
	$scope.endDate = new Date();
	$scope.catalogShown = false;
	$scope.plants = [];
	
	$scope.execQuery = function(){
		$http.get('/rest/plants', {params: {name: $scope.name, startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
			console.log(JSON.stringify(data));
			$scope.plants = data;
			$scope.catalogShown = true;
		});
	};
	
	$scope.setPlant = function (selectedPlant) {
        phr = { plant: selectedPlant, startDate: $scope.startDate, endDate: $scope.endDate};

        $http.post('/rest/prs', phr).success(function(data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
});

app.controller('AddPlantController', function($scope, $http, $location, $route, $routeParams){
	$scope.name = '';
	$scope.startDate = new Date();
	$scope.endDate = new Date();
	$scope.catalogShown = false;
	$scope.plants = [];
	
	$scope.execQuery = function(){
		$http.get('/rest/plants', {params: {name: $scope.name, startDate: $scope.startDate, endDate: $scope.endDate}}).success(function(data, status, headers, config) {
			console.log(JSON.stringify(data));
			$scope.plants = data;
			$scope.catalogShown = true;
		});
	};
	
	$scope.addPlant = function (selectedPlant) {
        phr = { plant: selectedPlant, startDate: $scope.startDate, endDate: $scope.endDate};

        $http.post('/rest/prs/'+$routeParams.id+'/lines', phr).success(function(data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
});

app.controller('PHRController', function($scope, $http, $route, $location) {
    $scope.phrs = [];
    $http.get('/rest/prs').success(function(data, status, headers, config) {
        console.log(data);
        $scope.phrs = data;
    });
    
    $scope.follow = function(id) {
    	$http.get('rest/prs/'+id+'/lines').success(function(data, status, headers, config) {
            console.log(data);
            $scope.phrs = data;
            $location.path('/phrs/'+id+'/lines');
        });
    };
    
    $scope.addPlant = function(id) {
    	$location.path('/phrs/'+id+'/addPlant')
    };
});

app.controller('PHRLinesController', function($scope, $http, $route, $location) {
    $scope.phrs = [];
    $http.get('/rest/phrs').success(function(data, status, headers, config) {
        console.log(data);
        $scope.phrs = data;
    });

    $scope.follow = function(link) {
        console.log(link);
        console.log(link.method);
        if (link.method == 'POST') {
            $http.post(link.href).success(function(data, status, headers, config) {
                console.log(data);
                $route.reload();
            });
        } else if (link.method == 'DELETE') {
            $http.delete(link.href).success(function(data, status, headers, config) {
                console.log(data);
                $route.reload();
            });             
        } else if (link.method == 'PUT') {
        	$http.put(link.href).success(function(data, status, headers, config) {
        		console.log(data);
                $route.reload();
        	});
        }
    };
});

app.controller('PHRLinesByPHRController', function($scope, $http, $route, $routeParams, $location) {
    $scope.phrs = [];
    $http.get('/rest/prs/'+ $routeParams.id + '/lines').success(function(data, status, headers, config) {
        console.log(data);
        $scope.phrs = data;
    });

    $scope.follow = function(link, requestID) {
        console.log(link);
        console.log(link.method);
        if(link.rel == 'update'){
        	$location.path('/phrs/update/'+requestID)
        }
        else if(link.rel == 'cancel'){
        	$location.path('/phrs/cancel/'+requestID)
        }
        else if(link.rel == 'requestExtension'){
        	$location.path('/phrs/extend/'+requestID)
        }
        else if (link.method == 'POST') {
            $http.post(link.href).success(function(data, status, headers, config) {
                console.log(data);
                $route.reload();
            });
        } else if (link.method == 'DELETE') {
            $http.delete(link.href).success(function(data, status, headers, config) {
                console.log(data);
                $route.reload();
            });             
        } else if (link.method == 'PUT') {
        	$http.put(link.href).success(function(data, status, headers, config) {
        		console.log(data);
                $route.reload();
        	});
        }
    };
    
    $scope.addPlant = function() {
    	$location.path('/phrs/'+$routeParams.id+'/addPlant');
    };
    
    $scope.approveAll = function() {
    	$http.post('/rest/prs/'+ $routeParams.id + '/approve').success(function(data, status, headers, config) {
    		$route.reload();
        });
    };
    
    $scope.inspectPlant = function(link) {
    	console.log(link);
    	$http.get(link).success(function(data, status, headers, config) {
    		console.log(link);
    		$scope.plant = data;
    		$location.path('/plants/'+$scope.plant.plantId);
    	});
    };
});

app.controller('UpdatePHRController', function($scope, $http, $route, $routeParams, $location){
	
	$http.get('/rest/phrs/'+ $routeParams.id).success(function(data, status, headers, config) {
        data.startDate = new Date(data.startDate);
        data.endDate = new Date(data.endDate);
        console.log(data);
        $scope.phr = data;
    });
	
	$scope.updatePHR = function (phr) {
        $http.put('/rest/phrs/' + $routeParams.id, phr).success(function(data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
});

app.controller('ExtendPHRController', function($scope, $http, $route, $routeParams, $location){
	
	$http.get('/rest/phrs/'+ $routeParams.id).success(function(data, status, headers, config) {
        data.startDate = new Date(data.startDate);
        data.endDate = new Date(data.endDate);
        console.log(data);
        $scope.phr = data;
        
        $scope.newDate = data.endDate;
    });
	
	$scope.extendPHR = function (phr, newDate) {
        $http.post('/rest/phrs/' + $routeParams.id + '/extensions', newDate).success(function(data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
})

app.controller('CancelPHRController', function($scope, $http, $route, $routeParams, $location){
	
	$http.get('/rest/phrs/'+ $routeParams.id).success(function(data, status, headers, config) {
        data.startDate = new Date(data.startDate);
        data.endDate = new Date(data.endDate);
        console.log(data);
        $scope.phr = data;
        
        $scope.phr._links.forEach(function(link) {
            if(link.rel == 'endPlantHireRequest')
        	{
            	$scope.message = 'A Purchase Order exists';
        	}
            
            if(link.rel == 'reject')
        	{
            	$scope.message = 'A Purchase Order has not been created';
        	}
        });
    });
	
	$scope.cancelPHR = function (id) {
        $http.delete('/rest/phrs/' + $routeParams.id).success(function(data, status, headers, config) {
            console.log(status);
            $location.path('/phrs');
        });
    };
})

app.controller("LoginController", function($scope, $http, $location, $rootScope, $cookieStore) {
    $scope.username = "";
    $scope.password = "";
    delete $rootScope.user;
    delete $rootScope.code;
    $cookieStore.remove("user");
    $cookieStore.remove("code");                
    $cookieStore.remove("userRoles");

    $scope.login = function () {
        var code = window.btoa($scope.username + ":" + $scope.password);
        console.log(code);
        $rootScope.user = $scope.username;
        $rootScope.code = code;
        $http.post("/rest/authentication")
            .success(function(data, status, headers, config) {
                console.log(data.roles);
                $cookieStore.put("userRoles", data.roles);
                $cookieStore.put("user", $scope.username);
                $cookieStore.put("code", code);             
                $location.path( "/phrs" );
            });
    };
});