package demo.services.rentit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.*;
import static org.springframework.test.web.client.response.MockRestResponseCreators.*;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.SimpleDbConfig;
import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantResource;
import demo.integration.dto.PurchaseOrderResource;
import demo.services.PlantHireRequestManager;
import demo.services.PlantNotAvailableException;
import demo.util.DateHelper;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
/*@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })*/
@ActiveProfiles("test")
//@ContextConfiguration
public class PlantCatalogServiceTests {
	@Configuration
	static class TestConfiguration {
		@Bean
		public RestTemplate restTemplate() {
			RestTemplate _restTemplate = new RestTemplate();
			List<HttpMessageConverter<?>> messageConverters = new ArrayList<HttpMessageConverter<?>>();
			messageConverters.add(new MappingJackson2HttpMessageConverter());
			_restTemplate.setMessageConverters(messageConverters);
			_restTemplate.setErrorHandler(new CustomResponseErrorHandler());
			return _restTemplate;
		}
		@Bean
		public RentalService rentalService() {
			return new RentalService();
		}
	}

	@Autowired
	private RentalService rentItProxy;
	@Autowired
	private RestTemplate restTemplate;

	//private MockRestServiceServer mockServer;
	
	@Autowired
    private WebApplicationContext wac;
	
	//private MockMvc mockMvc;

	@Before
	public void setup() {
		//this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
		//this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
	}

	@Test
	public void testFindAvailablePlants() throws Exception {
		Resource responseBody = new ClassPathResource("AvailablePlantsV1.json", this.getClass());
		ObjectMapper mapper = new ObjectMapper();
		List<PlantResource> list = mapper.readValue(responseBody.getFile(),mapper.getTypeFactory().constructCollectionType(List.class, PlantResource.class));

		/*mockServer.expect(
				requestTo("http://rentit.com/rest/plants?name=Excavator&startDate=2014-10-06&endDate=2014-10-10")
		)
				.andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(responseBody, MediaType.APPLICATION_JSON));*/

		List<PlantResource> result = rentItProxy.findAvailablePlants("Excavator", new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());
		
		//mockServer.verify();
		assertEquals(result, list);
	}

	@Test
	public void testCreatePurchaseOrder() throws Exception {
		
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
				request.getHeaders().add("prefer", "201");
				return execution.execute(request, body);
			}
		};
			 
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		
		ObjectMapper mapper = new ObjectMapper();

		PlantResource p = new PlantResource();
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setPlant(p);
		po.setStartDate(new LocalDate(2014, 10, 6).toDate());
		po.setEndDate(new LocalDate(2014, 10, 10).toDate());
		po.add(new Link("http://localhost/rest/pos/2", "self"));

		/*mockServer.expect(
				requestTo("http://rentit.com/rest/pos")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(po)))
				.andRespond(
						withCreatedEntity(new URI("http://rentit.com/rest/pos/345"))
						.body(mapper.writeValueAsString(po))
						.contentType(MediaType.APPLICATION_JSON)
						);*/

		PurchaseOrderResource result = rentItProxy.createPurchaseOrder(p, new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());

		//mockServer.verify();
		assertEquals(po, result);
	}
	
	@Test
	@DatabaseSetup(value="EmptyDatabase.xml")
	public void testQueryPlantsAndCreatePO() throws Exception{
		
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
				request.getHeaders().add("prefer", "201");
				return execution.execute(request, body);
			}
		};
			 
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		
		Resource responseBody = new ClassPathResource("AvailablePlantsV1.json", this.getClass());
		ObjectMapper mapper = new ObjectMapper();
		List<PlantResource> list = mapper.readValue(responseBody.getFile(),mapper.getTypeFactory().constructCollectionType(List.class, PlantResource.class));
		
		/*mockServer.expect(
				requestTo("http://rentit.com/rest/plants?name=Excavator&startDate=2014-10-06&endDate=2014-10-10")
		)
				.andExpect(method(HttpMethod.GET))
				.andRespond(withSuccess(responseBody, MediaType.APPLICATION_JSON));*/
		
		List<PlantResource> result = rentItProxy.findAvailablePlants("Excavator", new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());
		
		//mockServer.verify();
		assertEquals(result, list);
		
		PlantResource plant = result.get(0);
		
		PlantHireRequestLineResource phr = new PlantHireRequestLineResource();
		
		Date startDate = new LocalDate(2014, 10, 6).toDate();
		Date endDate = new LocalDate(2014, 10, 10).toDate();
		
		phr.setStartDate(startDate);
		phr.setEndDate(endDate);
		phr.setPlant(plant);
		phr.setCost(plant.getPrice() * (DateHelper.getDaysDifference(startDate, endDate)+1));
		phr.setRequestID(10005l);
		
		/*MvcResult result2 = mockMvc.perform(post("/rest/phrs")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(phr))
			)
	        	.andExpect(status().isCreated())
	        	// Verify the cost associated with the PHR
	        	.andReturn();*/
		
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setStartDate(po.getStartDate());
		po.setEndDate(po.getEndDate());
		po.setPlant(phr.getPlant());
		po.add(new Link("http://localhost/rest/pos/2", "self"));
		
		phr.setPurchaseOrder(po);
		
		/*this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
		
		mockServer.expect(
				requestTo("http://rentit.com/rest/pos")
		)
				.andExpect(method(HttpMethod.POST))
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(content().string(mapper.writeValueAsString(po)))
				.andRespond(
						withCreatedEntity(new URI("http://rentit.com/rest/pos/345"))
						.body(mapper.writeValueAsString(po))
						.contentType(MediaType.APPLICATION_JSON)
						);*/

		PurchaseOrderResource result2 = rentItProxy.createPurchaseOrder(plant, po.getStartDate(), po.getEndDate());
		
		//mockServer.verify();
		assertEquals(po, result2);
	}
	
	@Test(expected=PlantNotAvailableException.class)
	public void testQueryPlantsAndCreatePOReject() throws RestClientException, JsonParseException, JsonMappingException, IOException, PlantNotAvailableException{
		
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
				request.getHeaders().add("prefer", "409");
				return execution.execute(request, body);
			}
		};
			 
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		
		Resource responseBody = new ClassPathResource("AvailablePlantsV1.json", this.getClass());
		ObjectMapper mapper = new ObjectMapper();
		List<PlantResource> list = mapper.readValue(responseBody.getFile(),mapper.getTypeFactory().constructCollectionType(List.class, PlantResource.class));
		
		List<PlantResource> result = rentItProxy.findAvailablePlants("Excavator", new LocalDate(2014, 10, 6).toDate(), new LocalDate(2014, 10, 10).toDate());
		
		assertEquals(result, list);
		
		PlantResource plant = result.get(0);
		
		PlantHireRequestLineResource phr = new PlantHireRequestLineResource();
		
		Date startDate = new LocalDate(2014, 10, 6).toDate();
		Date endDate = new LocalDate(2014, 10, 10).toDate();
		
		phr.setStartDate(startDate);
		phr.setEndDate(endDate);
		phr.setPlant(plant);
		phr.setCost(plant.getPrice() * (DateHelper.getDaysDifference(startDate, endDate)+1));
		phr.setRequestID(10006l);
		
		PurchaseOrderResource po = new PurchaseOrderResource();
		po.setStartDate(po.getStartDate());
		po.setEndDate(po.getEndDate());
		po.setPlant(phr.getPlant());
		
		phr.setPurchaseOrder(po);
		
		rentItProxy.createPurchaseOrder(plant, po.getStartDate(), po.getEndDate());
	}
	
	@Test
	public void testRejectPO() throws Exception{
		Resource responseBody = new ClassPathResource("PurchaseOrderRejected.json", this.getClass());
		
		/*mockServer.expect(
				requestTo("http://rentit.com/rest/pos/10005/accept")
		)
				.andExpect(method(HttpMethod.DELETE))
				.andRespond(withSuccess(responseBody, MediaType.APPLICATION_JSON));*/
		
		PurchaseOrderResource poRej = rentItProxy.rejectPurchaseOrder(10005l);
		
		assertThat(poRej.getLink("self"), is(notNullValue()));
        //assertThat(poRej.getLink("updatePO"), is(notNullValue()));
		assertThat(poRej.get_link("updatePO"), is(notNullValue()));
        assertThat(poRej.getLinks().size() + poRej.get_links().size(), is(2));
	}
	
	@Test
	public void testUpdatePO() throws Exception{
		
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
				request.getHeaders().add("prefer", "200");
				return execution.execute(request, body);
			}
		};
		
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		
		Resource responseBody = new ClassPathResource("PurchaseOrderPending.json", this.getClass());
		
		/*mockServer.expect(
				requestTo("http://rentit.com/rest/pos/10005")
		)
				.andExpect(method(HttpMethod.PUT))
				.andRespond(withSuccess(responseBody, MediaType.APPLICATION_JSON));*/
		
		PurchaseOrderResource po = rentItProxy.updatePurchaseOrder(10005l, new PurchaseOrderResource());
		
		assertThat(po.getLink("self"), is(notNullValue()));
        assertThat(po.get_link("acceptPO"), is(notNullValue()));
		assertThat(po.get_link("rejectPO"), is(notNullValue()));
		assertThat(po.getLinks().size() + po.get_links().size(), is(3));
	}
	
	@Test(expected=PlantNotAvailableException.class)
	public void testUpdatePOReject() throws Exception{
		
		ClientHttpRequestInterceptor interceptor = new ClientHttpRequestInterceptor() {
			
			@Override
			public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
				request.getHeaders().add("prefer", "409");
				return execution.execute(request, body);
			}
		};
		
		restTemplate.setInterceptors(Collections.singletonList(interceptor));
		
		Resource responseBody = new ClassPathResource("PurchaseOrderPending.json", this.getClass());
		
		rentItProxy.updatePurchaseOrder(10005l, new PurchaseOrderResource());
	}
}
