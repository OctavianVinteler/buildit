package demo.integration.rest;

import java.util.Date;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.hateoas.Link;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import demo.Application;
import demo.SimpleDbConfig;
import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantResource;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class, SimpleDbConfig.class})
@WebAppConfiguration
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class })
@ActiveProfiles("test")
public class PHRRestControllerIntegrationTests {
    @Autowired
    private WebApplicationContext wac;
    
    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
	@Test
	@DatabaseSetup(value="EmptyDatabase.xml")
	public void testCreatePlantHireRequest() throws Exception {
		PlantResource plant = new PlantResource();
		plant.add(new Link("http://rentit.com/rest/plants/10001"));
		plant.setPrice(300f);
		
		PlantHireRequestLineResource phr = new PlantHireRequestLineResource();
		phr.setPlant(plant);
		phr.setStartDate(new LocalDate(2014, 10, 6).toDate());
		phr.setEndDate(new LocalDate(2014, 10, 10).toDate());
		
		MvcResult result = mockMvc.perform(post("/rest/phrs")
			.contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(phr))
		)
        	.andExpect(status().isCreated())
        	// Verify the cost associated with the PHR
        	.andReturn();
		
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("approve"), is(notNullValue()));
		assertThat(phrp.get_link("reject"), is(notNullValue()));
		assertThat(phrp.get_link("update"), is(notNullValue()));
		assertThat(phrp.get_link("cancel"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(5));		
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithPendingPHR.xml")
	public void testApprovePlantHireRequest() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("approve"), is(notNullValue()));

		Link approve = phrp.get_link("approve");
		
		result = mockMvc.perform(post(approve.getHref()))
				.andExpect(status().isAccepted())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("endPlantHireRequest"), is(notNullValue()));
		assertThat(phrp.get_link("requestExtension"), is(notNullValue()));
		assertThat(phrp.get_link("cancel"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(4));
		assertThat(phrp.getPurchaseOrder(), is(notNullValue()));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithPendingPHR.xml")
	public void testRejectPlantHireRequest() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("reject"), is(notNullValue()));

		Link reject = phrp.get_link("reject");
		
		result = mockMvc.perform(delete(reject.getHref()))
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(1));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithPendingPHR.xml")
	public void testUpdatePlantHireRequest() throws Exception {
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("update"), is(notNullValue()));

		Link update = phrp.get_link("update");
		
		result = mockMvc.perform(put(update.getHref())
				.contentType(MediaType.APPLICATION_JSON)
        		.content(mapper.writeValueAsString(phrp))
        		)
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("approve"), is(notNullValue()));
		assertThat(phrp.get_link("reject"), is(notNullValue()));
		assertThat(phrp.get_link("update"), is(notNullValue()));
		assertThat(phrp.get_link("cancel"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(5));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithApprovedPHR.xml")
	public void testRequestPHRExtension() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("requestExtension"), is(notNullValue()));

		Link request = phrp.get_link("requestExtension");
		
		result = mockMvc.perform(post(request.getHref())
				.contentType(MediaType.APPLICATION_JSON)
        		.content(mapper.writeValueAsString(new Date()))
				)
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("approveExtension"), is(notNullValue()));
		assertThat(phrp.get_link("rejectExtension"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(3));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithApprovedPHR.xml")
	public void testEndPHR() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("endPlantHireRequest"), is(notNullValue()));

		Link request = phrp.get_link("endPlantHireRequest");
		
		result = mockMvc.perform(delete(request.getHref()))
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(1));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithExtensionPending.xml")
	public void testRejectPHRExtension() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("rejectExtension"), is(notNullValue()));
		
		Link request = phrp.get_link("rejectExtension");
		
		result = mockMvc.perform(delete(request.getHref()))
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("endPlantHireRequest"), is(notNullValue()));
		assertThat(phrp.get_link("requestExtension"), is(notNullValue()));
		assertThat(phrp.get_link("cancel"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(4));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithExtensionPending.xml")
	public void testApprovePHRExtension() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("approveExtension"), is(notNullValue()));

		Link request = phrp.get_link("approveExtension");
		
		result = mockMvc.perform(post(request.getHref()))
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.get_link("endPlantHireRequest"), is(notNullValue()));
		assertThat(phrp.get_link("requestExtension"), is(notNullValue()));
		assertThat(phrp.get_link("cancel"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(4));
	}
	
	@Test
	@DatabaseSetup(value="DatabaseWithPendingPHR.xml")
	public void testCancelPHR() throws Exception{
		MvcResult result = mockMvc.perform(get("/rest/phrs/{id}", 10001L))
				.andExpect(status().isOk())
				.andReturn();
		PlantHireRequestLineResource phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.get_link("cancel"), is(notNullValue()));

		Link request = phrp.get_link("cancel");
		
		result = mockMvc.perform(delete(request.getHref()))
				.andExpect(status().isOk())
				.andReturn();
		
		phrp = mapper.readValue(result.getResponse().getContentAsString(), PlantHireRequestLineResource.class);
		assertThat(phrp.getLink("self"), is(notNullValue()));
		assertThat(phrp.getLinks().size() + phrp.get_links().size(), is(1));
	}
}
