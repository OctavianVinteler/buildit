Feature: Extend Plant Hire Request
  As a work engineer
  So that I modify th existing Plant Hire Request

  Background: Plant catalog
    Given the following plants are currently available for rental and a Plant Hire Request exists in the system with the status APPROVED
      | plantId | name      | description                      | price  |
      | 10001   | Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      | 10002   | Excavator | 3 Tonne Mini excavator           | 200.00 |
      | 10003   | Excavator | 5 Tonne Midi excavator           | 250.00 |
    And I am in the Plant Hire Request Extend web page for Plant Hire Request with id "1"

  Scenario: Extend the endDate of the Plant Hire Request
  	When I login and go to the Plant Hire Request Extend web page for Plant Hire Request with id "1"
    And I extend the plant hire request with a new end date of  "11-11-2014"
    Then I should have an extention with the end date set to "11-11-2014" and with the status PENDING
    
  Scenario: Extend the endDate of the Plant Hire Request
  	When I login and go to the Plant Hire Request Extend web page for Plant Hire Request with id "1"
    And I extend the plant hire request with a new end date of  "21-12-2014"
    Then I should have an extention with the end date set to "21-12-2014" and with the status PENDING
