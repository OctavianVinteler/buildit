package demo.ui;

import static org.junit.Assert.assertEquals;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantResource;
import demo.util.DateHelper;

public class ExtendPHRSteps {
	
    IDatabaseTester db = null;
	
	WebDriver user = null;
	
	
	@Before
    public void beforeScenario() throws ClassNotFoundException {
    	if (db == null) {
	    	db = new JdbcDatabaseTester("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/Course6DB", "postgres", "prodigy");
    	}
    	System.setProperty("webdriver.chrome.driver", "D:/Scoala/An 5 semestrul 1/Enterprise System Integration/Kits/chromedriver.exe");
    	user = new ChromeDriver();
    }
	
	@After
    public void afterScenario() throws Exception {
    	Statement st = db.getConnection().getConnection().createStatement();
    	st.executeUpdate("delete from plant_hire_request_extension");
    	st.executeUpdate("delete from plant_hire_request");
    	st.executeUpdate("delete from plant");
    	st.close();
    	user.close();
	}
	
	@Given("^the following plants are currently available for rental and a Plant Hire Request exists in the system with the status APPROVED$")
	public void the_following_plants_are_currently_available_for_rental_and_a_Plant_Hire_Request_exists_in_the_system_with_the_status_APPROVED(List<PlantResource> catalog) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
	    
		String sql = "insert into plant_hire_request (id, end_date, start_date, plant_ref, price, status) values (?, ?, ?, ?, ? ,?)";
		PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
		
        List<PlantResource> plants = new ArrayList<PlantResource>();
		
		for (int i = 0; i < catalog.size(); i++) {
			PlantResource plant = catalog.get(i);
			
			plants.add(plant);
		}
		
		PlantResource plant = plants.get(0);
		
		PlantHireRequestLineResource phr = new PlantHireRequestLineResource();
		phr.setPlant(plant);
		phr.setStartDate(new Date());
		phr.setEndDate(new Date());
		phr.setCost(plant.getPrice());
		
		ps.setInt(1, 1);
		ps.setDate(2, new java.sql.Date(phr.getEndDate().getTime()));
		ps.setDate(3, new java.sql.Date(phr.getStartDate().getTime()));
		ps.setString(4, "http://localhost/rest/plants/10002");
		ps.setFloat(5, phr.getCost());
		ps.setString(6, "APPROVED");
		ps.addBatch();
		
		ps.executeBatch();
		ps.close();
	}

	@Given("^I am in the Plant Hire Request Extend web page for Plant Hire Request with id \"(.*?)\"$")
	public void i_am_in_the_Plant_Hire_Request_Extend_web_page_for_Plant_Hire_Request_with_id(String id) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions

		user.get("http://localhost:8080/#phrs/extend/" + id);
	}
	
	@When("^I login and go to the Plant Hire Request Extend web page for Plant Hire Request with id \"(.*?)\"$")
	public void i_login_and_go_to_the_Plant_Hire_Request_Extend_web_page_for_Plant_Hire_Request_with_id(String id) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions

		Thread.sleep(2000);
		
		WebElement usernameBox = user.findElement(By.id("UsernameID"));
		usernameBox.sendKeys("site_e");
		
		WebElement passwordBox = user.findElement(By.id("PasswordID"));
		passwordBox.sendKeys("site_e");
		
		WebElement button = user.findElement(By.id("SubmitButtonID"));
		button.click();
		
		Thread.sleep(1500);
		
		user.get("http://localhost:8080/#phrs/extend/" + id);
	}
	
	@When("^I extend the plant hire request with a new end date of  \"(.*?)\"$")
	public void i_extend_the_plant_hire_request_with_a_new_end_date_of(String endDateString) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions

		Thread.sleep(2000);
		
		String[] endDateData = endDateString.split("-");
		
		WebElement endDateBox = user.findElement(By.id("NewDateValue"));
		endDateBox.sendKeys(endDateData[0] + endDateData[1]);
		endDateBox.sendKeys(Keys.TAB);
		endDateBox.sendKeys(endDateData[2]);
		
		WebElement button = user.findElement(By.id("ExtendButton"));
		button.click();
	}
	
	@Then("^I should have an extention with the end date set to \"(.*?)\" and with the status PENDING$")
	public void i_should_have_an_extention_with_the_end_date_set_to_and_with_the_status_PENDING(String newEndDate) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions

		Thread.sleep(1000);
		
		List<WebElement> rows = user.findElements(By.cssSelector(".tableRow"));
		assertEquals(1, rows.size());
		
		String sql = "select * from plant_hire_request_extension where phr_id=1";
		PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
		ResultSet r = ps.executeQuery();
		
		assertEquals(r.next(), true);
		
		Date endDate = r.getDate("end_date");
		String status = r.getString("status");
		
		assertEquals(status, "PENDING");
		assertEquals(DateHelper.getStringFromDate(endDate, "dd-MM-yyyy"), newEndDate);
	}
}
