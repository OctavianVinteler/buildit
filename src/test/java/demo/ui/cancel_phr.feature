Feature: Cancel Plant Hire Request
  As a work engineer
  So that I cancel an existing Plant Hire Request

  Scenario: Cancel the Plant Hire Request
  	Given the following plants are currently available for rental and a Plant Hire Request exists in the system with the status "PENDING"
      | plantId | name      | description                      | price  |
      | 10001   | Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      | 10002   | Excavator | 3 Tonne Mini excavator           | 200.00 |
      | 10003   | Excavator | 5 Tonne Midi excavator           | 250.00 |
    And I am in the Plant Hire Request Cancel web page for Plant Hire Request with id "1"
    When I login and go to the Plant Hire Request Cancel web page for Plant Hire Request with id "1"
    And I cancel the plant hire request
    Then The status of the Plant Hire Request should be "CANCELLED"
    
  Scenario: Cancel the Plant Hire Request
  	Given the following plants are currently available for rental and a Plant Hire Request exists in the system with the status "APPROVED"
      | plantId | name      | description                      | price  |
      | 10001   | Excavator | 1.5 Tonne Mini excavator         | 320.00 |
    And I am in the Plant Hire Request Cancel web page for Plant Hire Request with id "1"
    When I login and go to the Plant Hire Request Cancel web page for Plant Hire Request with id "1"
    And I cancel the plant hire request
    Then The status of the Plant Hire Request should be "CANCELLED"
