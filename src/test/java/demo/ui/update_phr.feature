Feature: Update Plant Hire Request
  As a work engineer
  So that I modify th existing Plant Hire Request

  Background: Plant catalog
    Given the following plants are currently available for rental and a Plant Hire Request exists in the system with the status PENDING
      | plantId | name      | description                      | price  |
      | 10001   | Excavator | 1.5 Tonne Mini excavator         | 150.00 |
      | 10002   | Excavator | 3 Tonne Mini excavator           | 200.00 |
      | 10003   | Excavator | 5 Tonne Midi excavator           | 250.00 |
    And I am in the Plant Hire Request Update web page for Plant Hire Request with id "1"
    
  Scenario: Update the Plant Hire Request with a 10 days interval
  	When I login and go to the Plant Hire Request Update web page for Plant Hire Request with id "1"
    And I modify the plant hire request start date with "02-11-2014" and end date with  "11-11-2014"
    Then I should have the new start date set to "2014-11-02" and end date set to "2014-11-11" and the price should be 1500
    
  Scenario: Update the Plant Hire Request with a 5 days interval
  	When I login and go to the Plant Hire Request Update web page for Plant Hire Request with id "1"
    And I modify the plant hire request start date with "06-12-2014" and end date with  "10-12-2014"
    Then I should have the new start date set to "2014-12-06" and end date set to "2014-12-10" and the price should be 750
