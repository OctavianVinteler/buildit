package demo.ui;

import java.io.Console;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import demo.integration.dto.PlantHireRequestLineResource;
import demo.integration.dto.PlantResource;
import demo.models.PlantHireRequestLine;

import org.openqa.selenium.By;

import static org.junit.Assert.*;

public class UpdatePHRSteps {
	
	IDatabaseTester db = null;
	
	WebDriver user = null;
	
	@Before
    public void beforeScenario() throws ClassNotFoundException {
    	if (db == null) {
	    	db = new JdbcDatabaseTester("org.postgresql.Driver", "jdbc:postgresql://localhost:5432/Course6DB", "postgres", "prodigy");
    	}
    	System.setProperty("webdriver.chrome.driver", "D:/Scoala/An 5 semestrul 1/Enterprise System Integration/Kits/chromedriver.exe");
    	user = new ChromeDriver();
    }
	
	@After
    public void afterScenario() throws Exception {
    	Statement st = db.getConnection().getConnection().createStatement();
    	st.executeUpdate("delete from plant_hire_request_extension");
    	st.executeUpdate("delete from plant_hire_request");
    	st.executeUpdate("delete from plant");
    	st.close();
    	user.close();
    }
	
	@Given("^the following plants are currently available for rental and a Plant Hire Request exists in the system with the status PENDING$")
	public void the_following_plants_are_currently_available_for_rental_and_a_Plant_Hire_Request_exists_in_the_system_with_the_status_PENDING(List<PlantResource> catalog) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
		
		String sql = "insert into plant_hire_request (id, end_date, start_date, plant_ref, price, status) values (?, ?, ?, ?, ? ,?)";
		PreparedStatement ps = db.getConnection().getConnection().prepareStatement(sql);
		
		List<PlantResource> plants = new ArrayList<PlantResource>();
		
		for (int i = 0; i < catalog.size(); i++) {
			PlantResource plant = catalog.get(i);
			
			plants.add(plant);
		}
		
		PlantResource plant = plants.get(0);
		
		PlantHireRequestLineResource phr = new PlantHireRequestLineResource();
		phr.setPlant(plant);
		phr.setStartDate(new Date());
		phr.setEndDate(new Date());
		phr.setCost(plant.getPrice());
		
		ps.setInt(1, 1);
		ps.setDate(2, new java.sql.Date(phr.getEndDate().getTime()));
		ps.setDate(3, new java.sql.Date(phr.getStartDate().getTime()));
		ps.setString(4, "http://localhost/rest/plants/10002");
		ps.setFloat(5, phr.getCost());
		ps.setString(6, "PENDING");
		ps.addBatch();
		
		ps.executeBatch();
		ps.close();
	}
	
	@Given("^I am in the Plant Hire Request Update web page for Plant Hire Request with id \"(.*?)\"$")
	public void i_am_in_the_Plant_Hire_Request_Update_web_page_for_Plant_Hire_Request_with_id(String id) throws Throwable {
		
		user.get("http://localhost:8080/#phrs/update/" + id);
	}
	
	@When("^I login and go to the Plant Hire Request Update web page for Plant Hire Request with id \"(.*?)\"$")
	public void i_login_and_go_to_the_Plant_Hire_Request_Update_web_page_for_Plant_Hire_Request_with_id(String id) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions

		Thread.sleep(2000);
		
		WebElement usernameBox = user.findElement(By.id("UsernameID"));
		usernameBox.sendKeys("work_e");
		
		WebElement passwordBox = user.findElement(By.id("PasswordID"));
		passwordBox.sendKeys("work_e");
		
		WebElement button = user.findElement(By.id("SubmitButtonID"));
		button.click();
		
		Thread.sleep(1500);
		
		user.get("http://localhost:8080/#phrs/update/" + id);
	}
	
	@When("^I modify the plant hire request start date with \"(.*?)\" and end date with  \"(.*?)\"$")
	public void i_modify_the_plant_hire_request_start_date_with_and_end_date_with(String startDateString, String endDateString) throws Throwable {
		
		Thread.sleep(2000);
		
		String[] startDateData = startDateString.split("-");
		String[] endDateData = endDateString.split("-");
		
		WebElement startDateBox = user.findElement(By.id("StartDateValue"));
		startDateBox.sendKeys(startDateData[0] + startDateData[1]);
		startDateBox.sendKeys(Keys.TAB);
		startDateBox.sendKeys(startDateData[2]);
		
		WebElement endDateBox = user.findElement(By.id("EndDateValue"));
		endDateBox.sendKeys(endDateData[0] + endDateData[1]);
		endDateBox.sendKeys(Keys.TAB);
		endDateBox.sendKeys(endDateData[2]);
		
		WebElement button = user.findElement(By.id("UpdateButton"));
		button.click();
	}
	
	@Then("^I should have the new start date set to \"(.*?)\" and end date set to \"(.*?)\" and the price should be (\\d+)$")
	public void i_should_have_the_new_start_date_set_to_and_end_date_set_to_and_the_price_should_be(String startDate, String endDate, int price) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
		Thread.sleep(1000);
		
		List<WebElement> rows = user.findElements(By.cssSelector(".tableRow"));
		assertEquals(1, rows.size());
		
		WebElement phrRow = rows.get(0);
		
		List<WebElement> columns = phrRow.findElements(By.xpath("td"));
		
		assertEquals(5, columns.size());
		
		WebElement startDateColumn = columns.get(1);
		WebElement endDateColumn = columns.get(2);
		WebElement costColumn = columns.get(3);
		
		assertEquals(startDateColumn.getText(), startDate);
		assertEquals(endDateColumn.getText(), endDate);
		assertEquals(Integer.parseInt(costColumn.getText()), price);
	}
}
