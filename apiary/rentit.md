FORMAT: 1A

# RentIt API
BuildIt API system

# Group Purchase Orders
PurchaseOrders related resources of the **Notes API**

## Purchase Order Management [/rest/pos]
### Retrieve Purchase Orders [GET]
+ Response 200 (application/json)

        [{
            "links":[
                { "rel":"self", "href":"http://localhost:3000/rest/pos/10001" }
            ],
            "_links":[
                { "rel":"closePO", "href":"http://localhost:3000/rest/pos/10001", "method":"DELETE" }
            ],
            "plant":{
                "links":[
                    { "rel":"self", "href":"http://localhost:3000/rest/plants/10001" }
                ],
                "_links":[]
            },
            "startDate":"2014-11-12",
            "endDate":"2014-11-14",
            "cost":750.0
        }]

### Create Purchase Order [POST]
+ Request (application/json)
        
        {
            "links":[],
            "_links":[],
            "poId":null,
            "startDate":1415224800000,
            "endDate":1415570400000,
            "plant":{
                "plantId":10001,
                "name":null,
                "description":null,
                "price":300.0
            },
            "updates":null,
            "id":null
        }


+ Response 201 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/2","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"rejectPO","href":"http://localhost/rest/pos/2/accept","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"acceptPO","href":"http://localhost/rest/pos/2/accept","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":2,
            "startDate":"2014-10-06",
            "endDate":"2014-10-10",
            "plant":{
                "plantId":10001,
                "name":"Excavator",
                "description":"1.5 Tonne Mini excavator",
                "price":150.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/2","variableNames":[],"variables":[],"templated":false},
            "cost":300.0
        }
        
+ Response 409 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/2","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"rejectPO","href":"http://localhost/rest/pos/2/accept","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"acceptPO","href":"http://localhost/rest/pos/2/accept","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":2,
            "startDate":"2014-10-06",
            "endDate":"2014-10-10",
            "plant":{
                "plantId":10001,
                "name":"Excavator",
                "description":"1.5 Tonne Mini excavator",
                "price":150.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/2","variableNames":[],"variables":[],"templated":false},
            "cost":300.0
        }
        
## Purchase Order Operations [/rest/pos/{id}]
A single PurchaseOrder object with all its details

+ Parameters
    + id (required, string, `"1"`) ... Numeric `id` of the PurchaseOrder to perform action with. Has example value.
    

### Retrieve Purchase Order [GET]

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/2","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"rejectPO","href":"http://localhost/rest/pos/2/accept","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"acceptPO","href":"http://localhost/rest/pos/2/accept","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":2,
            "startDate":1415224800000,
            "endDate":1415570400000,
            "plant":{
                "plantId":10001,
                "name":"Excavator",
                "description":"1.5 Tonne Mini excavator",
                "price":150.0
            },
            "updates":null,
            "id":{"rel":"self","href":"http://localhost/rest/pos/2","variableNames":[],"variables":[],"templated":false}
        }
        
### Update Purchase Order [PUT]

+ Request (application/json)
        
        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"updatePO","href":"http://localhost/rest/pos/10001","method":"PUT","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"rejectPO","href":"http://localhost/rest/pos/10001/accept","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"acceptPO","href":"http://localhost/rest/pos/10001/accept","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
        
+ Response 409 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"updatePO","href":"http://localhost/rest/pos/10001","method":"PUT","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
        
### Close Purchase Order [DELETE]

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
        
## Purchase Order Acceptance Operations [/rest/pos/{id}/accept]
A single PurchaseOrder object and the acceptance operations

+ Parameters
    + id (required, string, `"1"`) ... Numeric `id` of the PurchaseOrder to perform action with. Has example value.
    

### Accept Purchase Order [POST]

+ Request (application/json)
        
        

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"closePO","href":"http://localhost/rest/pos/10001","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"requestPOUpdate","href":"http://localhost/rest/pos/10001/updates","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator","price":450.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false},
            "cost":300.0
        }
        
### Reject Purchase Order [DELETE]

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"updatePO","href":"http://localhost/rest/pos/10001","method":"PUT","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
        
## Purchase Order Request Update Operation [/rest/pos/{id}/updates]
A single PurchaseOrderUpdate object creation

+ Parameters
    + id (required, string, `"1"`) ... Numeric `id` of the PurchaseOrder to perform action with. Has example value.

### Create Purchase Order Update [POST]

+ Request (application/json)
        
        

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"acceptPOUpdate","href":"http://localhost/rest/pos/10001/updates/1/accept","method":"POST","variableNames":[],"variables":[],"templated":false},
                {"rel":"rejectPOUpdate","href":"http://localhost/rest/pos/10001/updates/1/accept","method":"DELETE","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[
                {"id":1,"endDate":1411765200000,"status":"OPEN"}
            ],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
        
## Purchase Order Update Acceptance Operations [/rest/pos/{po.id}/updates/{up.id}/accept]
A single PurchaseOrderUpdate object acceptance methods

+ Parameters
    + po.id (required, string, `"1"`) ... Numeric `id` of the PurchaseOrder to perform action with. Has example value.
    + up.id (required, string, `"1"`) ... Numeric `id` of the PurchaseOrderUpdate to perform action with. Has example value.
    

### Accept Purchase Order Update [POST]

+ Request (application/json)
        
        

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"closePO","href":"http://localhost/rest/pos/10001","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"requestPOUpdate","href":"http://localhost/rest/pos/10001/updates","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[
                {"id":90009,"endDate":1411862400000,"status":"ACCEPTED"}
            ],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
    
### Reject Purchase Order Update [DELETE]

+ Response 200 (application/json)

        {
            "links":[
                {"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
            ],
            "_links":[
                {"rel":"closePO","href":"http://localhost/rest/pos/10001","method":"DELETE","variableNames":[],"variables":[],"templated":false},
                {"rel":"requestPOUpdate","href":"http://localhost/rest/pos/10001/updates","method":"POST","variableNames":[],"variables":[],"templated":false}
            ],
            "poId":10001,
            "startDate":1411171200000,
            "endDate":1411689600000,
            "plant":
            {
                "plantId":10006,
                "name":"Excavator",
                "description":"20 Tonne Large excavator",
                "price":450.0
            },
            "updates":[
                {"id":90009,"endDate":1411862400000,"status":"ACCEPTED"}
            ],
            "id":{"rel":"self","href":"http://localhost/rest/pos/10001","variableNames":[],"variables":[],"templated":false}
        }
        

        
        
        
        
        
        
        
        
        
        
        
# Group Plants
Plants related resources of the **BuildIt API**

## Plants Collection [/rest/plants]
### List all Plants [GET]
+ Response 200 (application/json)

        [{
                "id": 10002,
                "name": "Excavator",
                "description": "3 Tonne Mini excavator",
                "price": 200
            },
            {
                "id": 10004,
                "name": "Excavator",
                "description": "8 Tonne Midi excavator",
                "price": 300
            },
            {
                "id": 10005,
                "name": "Excavator",
                "description": "15 Tonne Large excavator",
                "price": 400
            }
        ]

### Create a Plant [POST]
+ Request (application/json)

        { "name": "Excavator", "description": "another one", "price": 300.00 }

+ Response 201 (application/json)

        { "plantId": "3", "name": "Excavator", "description": "another one", "price": 300.00 }

## Plants Queries [/rest/plants?name={name}&startDate={startDate}&endDate={endDate}]

+ Parameters
    + startDate (required, string, `"22-10-2014"`) ... The start date of the search interval
    + endDate (required, string, `"22-10-2014"`) ... The end date of the search interval
    + name (required, string, `"Excavator"`) ... The keyword to be included in the plant's name

### Get Plants by search interval [GET]

+ Response 200 (application/json)

        [{
                "id": 10002,
                "name": "Excavator",
                "description": "3 Tonne Mini excavator",
                "price": 200,
                "links":[
                    {"rel":"self","href":"http://localhost/rest/plants/10002","variableNames":[],"variables":[],"templated":false}
                ]
            },
            {
                "id": 10004,
                "name": "Excavator",
                "description": "8 Tonne Midi excavator",
                "price": 300,
                "links":[
                    {"rel":"self","href":"http://localhost/rest/plants/10004","variableNames":[],"variables":[],"templated":false}
                ]
            },
            {
                "id": 10005,
                "name": "Excavator",
                "description": "15 Tonne Large excavator",
                "price": 400,
                "links":[
                    {"rel":"self","href":"http://localhost/rest/plants/10005","variableNames":[],"variables":[],"templated":false}
                ]
            }
        ]

## Plant [/rest/plants/{id}]
A single Plant object with all its details

+ Parameters
    + id (required, string, `"1"`) ... Numeric `id` of the Plant to perform action with. Has example value.

### Retrieve a Plant [GET]
+ Response 200 (application/json)

    + Header

            

    + Body

            { "plantId": "2", "name": "Dumper", "description": "powerful", "price": 450.00 }

### Remove a Plant [DELETE]
+ Response 204

### Update a Plant [PUT]

+ Request (application/json)

        { "plantId": "2", "name": "Excavator", "description": "another one", "price": 300.00 }

+ Response 200 (application/json)

    + Header

            

    + Body

            { "plantId": "2", "name": "Dumper", "description": "another one", "price": 300.00 }
